package dto

import "encoding/json"

type PageParam struct {
	Current  int32 `json:"current"`
	PageSize int32 `json:"page_size"`
	Total    int32 `json:"total"`
	Count    int   `json:"count"`
}

func (pageParam *PageParam) String() string {
	s, _ := json.Marshal(pageParam)
	return string(s)

}

func (pageParam *PageParam) ToString() string {
	s, _ := json.MarshalIndent(pageParam, "", " ")
	return string(s)

}

type PageResult struct {
	JsonResult
	Page PageParam     `json:"data"`
	Data []interface{} `json:"data"`
}

func (pageResult *PageResult) String() string {
	s, _ := json.Marshal(pageResult)
	return string(s)

}
func (pageResult *PageResult) ToString() string {
	s, _ := json.MarshalIndent(pageResult, "", " ")
	return string(s)

}
