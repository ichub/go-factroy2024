package dto

import (
	"encoding/json"
	"gitee.com/ichub/go-factroy2024/common/base/model"
)

const CURRENT = 0
const PAGE_SIZE = 20

type QueryParam struct {
	Current    int32   `json:"current"`
	PageSize   int32   `json:"page_size"`
	OrderBys   *string `json:"order_bys"`
	FuzzyQuery bool    `json:"fuzzy_query"`
	EsQuery    bool    `json:"es_query"`

	Param model.ParamBase `json:"param"`
}

func (param *QueryParam) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

func (param *QueryParam) ToString() string {
	s, _ := json.MarshalIndent(param, "", " ")
	return string(s)

}
