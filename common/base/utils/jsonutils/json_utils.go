package jsonutils

import (
	"encoding/json"
	"fmt"
)

type JsonUtils struct {
}

func ToJsonStr(v any) string {
	result, err := json.Marshal(v)
	if err != nil {
		fmt.Println(err)
		return "{}"
	}
	return string(result)
}

func ToJsonPretty(v any) string {
	result, err := json.MarshalIndent(v, "", "\t")
	if err != nil {
		fmt.Println(err)
		return "{}"
	}
	return string(result)
}

//json.Unmarshal(bs, &a1)
//	json.Unmarshal(result, &ret)
func JsonFrom(param []byte) any {
	var ret any
	_ = json.Unmarshal(param, &ret)
	return ret
}
func JsonFromAny(param []byte, ret *any) error {

	return json.Unmarshal(param, ret)

}
func JsonFromMap(param []byte, ret *map[string]any) error {

	return json.Unmarshal(param, ret)

}
