package model

import "encoding/json"

type ParamBase struct {
	DateRanges   map[string][]int64  `json:"date_ranges,string"`
	IntRanges    map[string][]int64  `json:"int_ranges,string"`
	StringRanges map[string][]string `json:"string_ranges"`

	InRanges map[string]string `json:"in_ranges"`
}

func (pb *ParamBase) Ini() {
	pb.DateRanges = make(map[string][]int64)
	pb.IntRanges = make(map[string][]int64)
	pb.StringRanges = make(map[string][]string)
	pb.InRanges = make(map[string]string)

}

func (param *ParamBase) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

func (param *ParamBase) ToString() string {
	s, _ := json.MarshalIndent(param, "", " ")
	return string(s)

}
