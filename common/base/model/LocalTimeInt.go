package model

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"time"
)

type LocalTimeInt struct {
	time.Time
}

func (t LocalTimeInt) MarshalJSON() ([]byte, error) {
	//格式化秒
	seconds := t.Unix()
	return []byte(strconv.FormatInt(seconds, 10)), nil
	//return []byte(fmt.Sprintf("%d",seconds)), nil
}

func (t *LocalTimeInt) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		t.Time = time.Unix(0, 0)
		return nil
	}

	i64, err := strconv.ParseInt(string(data), 10, 64)
	t.Time = time.Unix(i64, 0)
	return err
}

func (t LocalTimeInt) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}
func (t *LocalTimeInt) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = LocalTimeInt{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}

func (t *LocalTimeInt) FormatDatetime() string {
	return t.Time.Format("2006-01-02 15:04:05")
}

func (t *LocalTimeInt) FormatDate() string {
	return t.Time.Format("2006-01-02")
}

func (t *LocalTimeInt) Zero() LocalTimeInt {
	return LocalTimeInt{}
}
