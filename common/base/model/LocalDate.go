package model

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"
)

type LocalDate struct {
	time.Time
}

func (t *LocalDate) MarshalJSON() ([]byte, error) {
	//格式化秒seconds := t.Unix() return []byte(strconv.FormatInt(seconds, 10)), nil
	tune := t.Format("20060102")
	return []byte(tune), nil
}

func (t *LocalDate) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}
	//var err error
	//前端接收的时间字符串
	str := string(data)
	//去除接收的str收尾多余的"
	timeStr := strings.Trim(str, "\"")
	t1, err := time.Parse("20060102", timeStr)
	t.Time = t1
	return err
}

func (t *LocalDate) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}
func (t *LocalDate) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = LocalDate{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}

func (t *LocalDate) FormatDate() string {
	return t.Time.Format("2006-01-02")
}

//DateTemplate = "2006-01-02"
//TimeTemplate = "2006-01-02 15:04:05"
