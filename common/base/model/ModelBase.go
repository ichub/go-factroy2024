package model

import "encoding/json"

type ModelBase struct {
	// gorm.Model
	// Id        *uint64     `gorm:"primary_key" json:"id"`
	CreatedAt *LocalDateInt `json:"createdAt"`
	UpdatedAt *LocalDateInt `json:"updatedAt"`
	DeletedAt *LocalDateInt `gorm:"column:deleted_at;type:timestamp" sql:"index" json:"-"`
	CreatedBy *uint64       `json:"createBy"`
	UpdatedBy *uint64       `json:"updatedBy"`
	DeletedBy *uint64       `json:"deletedBy"`
}

func (entity *ModelBase) String() string {
	s, _ := json.Marshal(entity)
	return string(s)

}

func (entity *ModelBase) ToString() string {
	s, _ := json.MarshalIndent(entity, "", " ")
	return string(s)

}
