package base

import "fmt"

type CmsError struct {
	Code int32
	Msg  string
}

func (ce *CmsError) Error() string {
	return fmt.Sprintf(ce.Msg+" :%d ", ce.Code)
}
