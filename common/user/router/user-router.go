package router

import (
	"github.com/gin-gonic/gin"
	"gitee.com/ichub/go-factroy2024/common/shop/controller"
	auth2 "gitee.com/ichub/go-factroy2024/common/user/controller/auth"
	middleware2 "gitee.com/ichub/go-factroy2024/common/user/middleware"
)

func SetupUserRouter() *gin.Engine {
	v1 := gin.Default()

	v1.POST("/api/auth/register", auth2.Register)
	v1.POST("/api/auth/login", auth2.Login)
	v1.GET("/api/auth/info", middleware2.AuthMiddleware(), auth2.Info)
	v1.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	v1.GET("/dpt/kunlong/departmentService/query", controller.InstDepartmentController.Query)
	v1.GET("/api/department/findById", controller.InstDepartmentController.FindById)

	/*	v1.GET("/api/employee/query", employee2.Query)*/
	v1.GET("/api/employee/query", controller.InstEmployeeController.Query)
	v1.GET("/api/employee/findById", controller.InstEmployeeController.FindById)

	v1.DELETE("/api/ichub/icd/cms/shop-code/deleteById", controller.InstEmployeeController.DeleteById)
	v1.POST("/api/ichub/icd/cms/shop/save", controller.InstEmployeeController.Save)
	//v1.POST("/api/ichub/icd/cms/partment/save", employee2.Save)
	return v1

}
