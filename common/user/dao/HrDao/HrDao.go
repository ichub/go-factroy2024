package HrDao

import (
	"github.com/jinzhu/gorm"
	"gitee.com/ichub/go-factroy2024/common/dbcontent"
	dto2 "gitee.com/ichub/go-factroy2024/common/user/dto"
	model2 "gitee.com/ichub/go-factroy2024/common/user/model"
	"strings"
)

func buildWhere(queryParam dto2.HrQueryParam, db *gorm.DB) *gorm.DB {

	if queryParam.Param.Id != nil {
		db = db.Where("id=?", *queryParam.Param.Id)
	}

	if queryParam.Param.Name != nil {
		db = db.Where("name=?", *queryParam.Param.Name)
	}

	if queryParam.Param.Phone != nil {
		db = db.Where("phone=?", *queryParam.Param.Phone)
	}

	if queryParam.Param.Telephone != nil {
		db = db.Where("telephone=?", *queryParam.Param.Telephone)
	}

	if queryParam.Param.Address != nil {
		db = db.Where("address=?", *queryParam.Param.Address)
	}

	if queryParam.Param.Enabled != nil {
		db = db.Where("enabled=?", *queryParam.Param.Enabled)
	}

	if queryParam.Param.Username != nil {
		db = db.Where("username=?", *queryParam.Param.Username)
	}

	if queryParam.Param.Password != nil {
		db = db.Where("password=?", *queryParam.Param.Password)
	}

	if queryParam.Param.Userface != nil {
		db = db.Where("userface=?", *queryParam.Param.Userface)
	}

	if queryParam.Param.Remark != nil {
		db = db.Where("remark=?", *queryParam.Param.Remark)
	}

	return db
}
func FindById(id int32) *model2.Hr {

	var model *model2.Hr = new(model2.Hr)
	dbcontent.GetDB().First(model, id)

	return model

}
func CountByQueryParam(queryParam dto2.HrQueryParam) int32 {

	var c int32
	db := dbcontent.GetDB().Model(&model2.Hr{}).Limit(1).Offset(0)
	db = buildWhere(queryParam, db)

	db.Count(&c)

	return c
}
func FindByQueryParam(queryParam dto2.HrQueryParam) []model2.Hr {

	var models []model2.Hr
	db := dbcontent.GetDB().Model(&model2.Hr{}).Limit(queryParam.Limit).Offset(queryParam.Start)
	db = buildWhere(queryParam, db)

	if queryParam.OrderBys != nil {
		rs := strings.Split(*queryParam.OrderBys, ",")
		for _, value := range rs {
			orders := strings.Split(value, "|")
			db = db.Order(strings.Join(orders, " "))
		}
	}

	db.Find(&models)
	return models
}
func DeleteById(id int32) {
	var model model2.Hr

	dbcontent.GetDB().Where("id=?", id).Delete(&model)

}

func Insert(model *model2.Hr) int32 {

	dbcontent.GetDB().Save(&model)

	return *model.Id
}

func Update(model *model2.Hr) int32 {

	dbcontent.GetDB().Save(model)

	return (*model.Id)
}

func FindByIds(pks []string) []model2.Hr {
	var models []model2.Hr
	dbcontent.GetDB().Raw("select * from hr where id in (" + strings.Join(pks, ",") + ")").Scan(&models)

	return models
}

/*hr := hrDao.FindById(3)
fmt.Println(*hr.Name)

var qp dto.HrQueryParam
qp.Param = new(model.Hr)
qp.Ini()

hrs := hrDao.FindByQueryParam(qp)
for _, v := range hrs {
	if v.Name != nil {
		fmt.Println(*v.Name)
	}
}
fmt.Println(len(hrs))
s,_:=json.Marshal(qp)
fmt.Println(string(s))

var hrr model.Hr
id := hrDao.Insert(&hrr)
hrr.Id = nil
id1 := hrDao.Insert(&hrr)
fmt.Println(id)

hr.Id = &id1
hrDao.Update(hr)

hrDao.DeleteById(id)*/
