package auth

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	util2 "gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/common/dbcontent"
	dto2 "gitee.com/ichub/go-factroy2024/common/user/dto"
	model2 "gitee.com/ichub/go-factroy2024/common/user/model"
	response2 "gitee.com/ichub/go-factroy2024/common/user/response"

	//"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

func Login(ctx *gin.Context) {
	//获取参数
	DB := dbcontent.GetDB()
	telephone := ctx.PostForm("telephone")
	password := ctx.PostForm("password")
	if len(telephone) != 11 {
		ctx.JSON(http.StatusUnprocessableEntity, gin.H{"code": 422, "msg": "手机号必须11位"})
		return
	}
	if len(password) < 6 {
		ctx.JSON(http.StatusUnprocessableEntity, gin.H{"code": 422, "msg": "密码不能少于6位"})
		return
	}
	var user model2.CmsUser
	DB.Where("telephone=?", telephone).First(&user)
	if user.ID == 0 {
		ctx.JSON(http.StatusUnprocessableEntity, gin.H{"code": 422, "msg": "用户不存在"})
		return
	}
	//判断密码验收是否正确
	/*	if err := bcrypt.CompareHashAndPassword([]byte(shop-code.Password), []byte(password)); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"code": 400, "msg": "密码错误"})
		return
	}*/
	//发放token
	token, err := dbcontent.ReleaseToken(user)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "系统异常"})
		log.Print("token generate error:%v", err)
		return
	}
	response2.Success(ctx, gin.H{"token": token}, "注册成功")

}
func Info(ctx *gin.Context) {
	user, _ := ctx.Get("shop-code")
	ctx.JSON(http.StatusOK, gin.H{"code": 200, "data": gin.H{"shop-code": dto2.ToUserDto(user.(model2.CmsUser))}})

}
func Register(ctx *gin.Context) {
	//获取参数
	DB := dbcontent.GetDB()
	name := ctx.PostForm("name")
	telephone := ctx.PostForm("telephone")
	fmt.Printf("手机号是:%v", telephone)
	//数据验证
	password := ctx.PostForm("password")

	//判断手机号是否存在
	if len(telephone) != 11 {
		response2.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "手机号必须11位")
		return
	}
	if len(password) < 6 {
		response2.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "密码不能少于6位")
		return
	}
	if isTelephoneExists(DB, telephone) {
		response2.Response(ctx, http.StatusUnprocessableEntity, 422, nil, "用户手机号已经存在")

	}
	if len(name) == 0 {
		name = util2.RandomString(10)
	}
	hasedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		response2.Response(ctx, http.StatusUnprocessableEntity, 500, nil, "加密错误")

		return
	}
	//创建用户
	newUser := model2.CmsUser{
		Name:      name,
		Telephone: telephone,
		Password:  string(hasedPassword),
	}
	DB.Create(&newUser)
	//如果名称没有传

	fmt.Println(name, telephone, password)
	//创建用户

	response2.Success(ctx, nil, "注册成功")
}
func isTelephoneExists(db *gorm.DB, telephone string) bool {
	var user model2.CmsUser
	db.Where("telephone=?", telephone).First(&user)
	if user.ID != 0 {
		return true
	}
	return false
}
