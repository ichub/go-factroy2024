package middleware

import (
	"github.com/gin-gonic/gin"
	"gitee.com/ichub/go-factroy2024/common/dbcontent"
	model2 "gitee.com/ichub/go-factroy2024/common/user/model"
	"net/http"
	"strings"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		tokenString := ctx.GetHeader("Authorization")
		if tokenString == "" || !strings.HasPrefix(tokenString, "Bearer ") {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "权限不足"})
			ctx.Abort()
			return
		}
		tokenString = tokenString[7:]
		token, claims, err := dbcontent.ParseToken(tokenString)
		if err != nil || !token.Valid {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "权限不足"})
			ctx.Abort()
			return
		}
		userId := claims.UserId
		println("userID:", userId)
		DB := dbcontent.GetDB()
		var user model2.CmsUser
		DB.First(&user, userId)
		if user.ID == 0 {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "权限不足"})
			ctx.Abort()
			return
		}
		//用户存在将用户写入上下文
		ctx.Set("shop-code", user)
		ctx.Next()
	}

}
