package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"gitee.com/ichub/go-factroy2024/common/base/dto"
	"gitee.com/ichub/go-factroy2024/common/dbcontent"
	dto2 "gitee.com/ichub/go-factroy2024/common/shop/dto"
	model2 "gitee.com/ichub/go-factroy2024/common/shop/model"
	"gitee.com/ichub/go-factroy2024/common/shop/router"
	"gitee.com/ichub/go-factroy2024/common/shop/service"
	"os"
	"time"
)

func main() {
	fmt.Println(viper.AllSettings())
	db := dbcontent.InitDB()
	defer db.Close()

	engine := router.InitRouter()
	var employees []model2.Employee

	port := viper.GetString("publisher.port")

	table := db.Table("employee")
	var employee dto2.EmployeeParam
	i := int32(91)
	employee.DepartmentId = &i
	if *employee.DepartmentId > 0 {
		table.Where("departmentId = ?", 91).Offset(0).Limit(5).Find(&employees)
	}

	h := gin.H{"code": 200}
	fmt.Println(h)
	var dict map[string]string     //定义dict为map类型
	dict = make(map[string]string) //让dict可编辑
	dict["a"] = "a"
	fmt.Println(dict)

	var es []model2.Employee
	db.Raw("select * from department", es)
	fmt.Printf("%d", len(es))
	for _, v := range es { //timeStr := v.Birthday.Format("2006-01-02 15:04:05")

		fmt.Println(v.String())

	}

	//dbfactroy := dbtool2.TableTool{"vhr","employee"}
	//dbfactroy.FindColumns()
	//cnt:=employeeService.CountByQueryParam()
	//fmt.Println(cnt)
	pageresult := dto.PageResult{}

	for _, data := range employees {
		//ss, _ :=json.Marshal(data)

		//fmt.Println(string(ss))
		pageresult.Data = append(pageresult.Data, data)
	}
	for _, data := range pageresult.Data {

		result, _ := data.(model2.Employee) //通过断言实现类型转换
		s, _ := json.Marshal(result)
		fmt.Println(string(s))

		pageresult.Data = append(pageresult.Data, data)
	}

	ret := service.InstEmployeeService.FindById(2)
	fmt.Println("FindById 1 jsonResult===")
	fmt.Println(ret.String())

	qq := dto2.EmployeeQueryParam{}
	employee.SetId(11)
	qq.Param = &employee
	qq.PageSize = 5
	qq.Current = 1

	fmt.Println(qq.String())
	ars := service.InstEmployeeService.Query(&qq)

	for _, data := range ars.GetData() {
		fmt.Println(data.String)
	}

	if port != "" {
		panic(engine.Run(":" + port))
	}
	panic(engine.Run())
}

func init() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)

	}
	//else {
	//	fmt.Println(viper.AllSettings())
	//}
	return
}

/*func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	return r
}*/

func t() {
	now := time.Now()
	timeStr := now.Format("2006-01-02 15:04:05")
	fmt.Println(timeStr)

}
