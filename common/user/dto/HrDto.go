package dto

import (
	"gitee.com/ichub/go-factroy2024/common/base/dto"
	model2 "gitee.com/ichub/go-factroy2024/common/user/model"
)

type HrQueryParam struct {
	OrderBys *string    `json:"orderBys"`
	Param    *model2.Hr `json:"param"`

	Start int `json:"start"`
	Limit int `json:"limit"`
}

func (qp *HrQueryParam) Ini() {
	qp.Start = 0
	qp.Limit = 20
	qp.Param = new(model2.Hr)
}

type HrPageResult struct {
	dto.PageResult
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Count   int         `json:"count"`
	Total   int32       `json:"total"`
	Data    []model2.Hr `json:"data"`
}

func (p *HrPageResult) SetData(s []model2.Hr) {
	p.Data = s
}

func (p *HrPageResult) GetData() []model2.Hr {

	return p.Data
}
