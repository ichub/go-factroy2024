package model

type HrBase struct{}
type Hr struct {
	HrBase
	Id        *int32  `gorm:"column:id" json:"id"`
	Name      *string `gorm:"column:name" json:"name"`
	Phone     *string `gorm:"column:phone" json:"phone"`
	Telephone *string `gorm:"column:telephone" json:"telephone"`
	Address   *string `gorm:"column:address" json:"address"`
	Enabled   *int8   `gorm:"column:enabled" json:"enabled"`
	Username  *string `gorm:"column:username" json:"username"`
	Password  *string `gorm:"column:password" json:"password"`
	Userface  *string `gorm:"column:userface" json:"userface"`
	Remark    *string `gorm:"column:remark" json:"remark"`
}

func (*Hr) TableName() string {

	return "hr"
}
