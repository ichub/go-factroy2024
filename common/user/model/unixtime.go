package model

import (
	"strconv"

	"github.com/golang/protobuf/ptypes/timestamp"
)

type UnixTime struct {
	timestamp.Timestamp
}

func (t UnixTime) MarshalJSON() ([]byte, error) {
	stamp := ""
	if t.Seconds == 0 {
		stamp = "\"\""
	} else {
		stamp = "\"" + strconv.FormatInt(t.Seconds, 10) + "\""
	}
	return []byte(stamp), nil
}
