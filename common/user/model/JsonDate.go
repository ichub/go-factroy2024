package model

import (
	"fmt"
	"time"
)

const (
	TimeTemplate = "2006-01-02 15:04:05"
	DateTemplate = "2006-01-02"

	UTCTimeTemplate = "2006-01-02T15:04:05.000Z"
)

type Date struct {
	time.Time
}

func (d *Date) MarshalJSON() ([]byte, error) {

	return []byte(fmt.Sprintf(`"%s"`, d.Local().Format(DateTemplate))), nil
}

func (d *Date) UnmarshalJSON(b []byte) error {

	// 指定时区
	//loc, _:= time.LoadLocation("Asia/Chongqing")
	dd, err := time.ParseInLocation(`"2006-01-02"`, string(b), time.Local)
	fmt.Println(d.Time.Format("2006-01-02"))
	//d.Time, err = time.ParseInLocation(DateTemplate, string(b))
	if err != nil {
		return err
	}
	d.Time = dd
	return nil
}

type Datetime struct {
	time.Time
}

func (d Datetime) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%s", d.Format(TimeTemplate))), nil
}

func (d Datetime) UnmarshalJSON(b []byte) error {

	dd, err := time.Parse(TimeTemplate, string(b))
	if err != nil {
		return err
	}
	d.Time = dd

	return nil
}

type DatetimeUTC struct {
	time.Time
}

func (d *DatetimeUTC) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%s", d.Format(UTCTimeTemplate))), nil
}

func (d *DatetimeUTC) UnmarshalJSON(b []byte) error {

	dd, err := time.Parse(UTCTimeTemplate, string(b))
	if err != nil {
		return err
	}
	d.Time = dd

	return nil
}
