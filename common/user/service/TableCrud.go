package service

type TableCrud interface {
	DeleteById()
	FindById()
	Insert()
	Update()

	FindByQueryParam()
	CountByQueryParam()
}
