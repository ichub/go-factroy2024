package testcasefactroy

import (
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"strings"
	"time"
)

/*
   @Title    文件名称: go_factroy.go
   @Description  描述: 代码工厂测试用例文件生成

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type TestcaseFactroy struct {
}

func (factroy *TestcaseFactroy) buildService(dbFactroy *dbfactroy.DbFactroy, buildEs bool) string {
	var bs interface{}
	if buildEs {
		bs = filefactroy.FileFactroyInst.ReadTestTemplateESSrv()
	} else {
		bs = filefactroy.FileFactroyInst.ReadTestTemplateSrv()
	}

	vars := make(map[string]interface{})
	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Srv_test.go"
	vars["Description"] = "测试服务" + utils.Case2Camel(dbFactroy.Table) + "Srv"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbFactroy.Table)
	vars["ESModelName"] = utils.Case2Camel(dbFactroy.Table)
	if buildEs {
		vars["ESModelName"] = "ES" + vars["ESModelName"].(string)
	}
	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindGoType(dbFactroy.PkeyType)
	vars["pkeyField"] = dbFactroy.Pkey

	vars["StringFieldName"] = utils.Case2Camel(dbFactroy.StringFieldName)
	vars["StringFieldLen"] = dbFactroy.StringFieldLen

	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbFactroy.FindGoType(dbFactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	} else if dbFactroy.FindGoType(dbFactroy.PkeyType) == "model.BitField" {
		vars["PkeyValue"] = "false"
		vars["fmtType"] = "%s"
	}
	s := string(bs.([]byte))
	if !buildEs {
		s = strings.ReplaceAll(s, "esservice", "service")
	}
	return utils.ParseTemplateString(s, vars)

}

func (factroy *TestcaseFactroy) buildController(dbFactroy *dbfactroy.DbFactroy) string {
	bs := filefactroy.FileFactroyInst.ReadTestTemplateController()

	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Controller_test.go"
	vars["Description"] = "测试API" + utils.Case2Camel(dbFactroy.Table) + "Controller"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbFactroy.Table)
	vars["lsTableName"] = strings.ToLower(utils.Case2Camel(dbFactroy.Table))
	vars["ESModelName"] = utils.Case2Camel(dbFactroy.Table)

	vars["Pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindGoType(dbFactroy.PkeyType)
	vars["pkeyField"] = dbFactroy.Pkey

	vars["StringFieldName"] = utils.Case2Camel(dbFactroy.StringFieldName)
	vars["StringFieldLen"] = dbFactroy.StringFieldLen

	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbFactroy.FindGoType(dbFactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	} else if dbFactroy.FindGoType(dbFactroy.PkeyType) == "model.BitField" {
		vars["PkeyValue"] = "false"
		vars["fmtType"] = "%s"
	}
	s := string([]byte(bs))

	return utils.ParseTemplateString(s, vars)

}
func (factroy *TestcaseFactroy) BuildTestServiceFile(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDirTest(filefactroy.FileFactroyInst.GendirServiceTest, fbase)

	f, c = fbase+"Srv_test.go", factroy.buildService(dbFactroy, false)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}

func (factroy *TestcaseFactroy) BuildTestESServiceFile(dbFactroy *dbfactroy.DbFactroy) (f string, c string) {
	var contents []string

	contents = append(contents, factroy.buildService(dbFactroy, true))

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	tb := fbase + "ESSrv"
	f, c = tb+"_test.go", strings.Join(contents, "\n")

	sdir := filefactroy.FileFactroyInst.TransDirTest(filefactroy.FileFactroyInst.GendirEsserviceTest, fbase)

	filefactroy.FileFactroyInst.WriteFileModule(sdir, f, c)

	return f, c
}

func (factroy *TestcaseFactroy) buildDAOService(dbFactroy *dbfactroy.DbFactroy) string {
	var bs interface{}

	bs = filefactroy.FileFactroyInst.ReadTestTemplateDAO()

	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Dao_test.go"
	vars["Description"] = "测试服务" + utils.Case2Camel(dbFactroy.Table) + "Dao"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbFactroy.Table)
	vars["ESModelName"] = utils.Case2Camel(dbFactroy.Table)

	//	vars["ESModelName"] = "ES" + vars["ESModelName"].(string)
	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindGoType(dbFactroy.PkeyType)

	vars["StringFieldName"] = utils.Case2Camel(dbFactroy.StringFieldName)
	vars["StringFieldLen"] = dbFactroy.StringFieldLen

	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbFactroy.FindGoType(dbFactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	}
	s := string(bs.([]byte))

	s = strings.ReplaceAll(s, "esservice", "service")

	return utils.ParseTemplateString(s, vars)

}

func (factroy *TestcaseFactroy) BuildTestDAOFile(dbFactroy *dbfactroy.DbFactroy) (f string, c string) {
	var contents []string

	contents = append(contents, factroy.buildDAOService(dbFactroy))

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	tb := fbase + "DAO"
	f, c = tb+"_test.go", strings.Join(contents, "\n")

	sdir := filefactroy.FileFactroyInst.TransDirTest(filefactroy.FileFactroyInst.GendirDaoTest, fbase)

	filefactroy.FileFactroyInst.WriteFileModule(sdir, f, c)

	return f, c
}

func (factroy *TestcaseFactroy) BuildTestControllerFile(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDirTest(filefactroy.FileFactroyInst.GendirControllerTest, fbase)

	f, c = fbase+"Controller_test.go", factroy.buildController(dbFactroy)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}
func (factroy *TestcaseFactroy) Build(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {
	factroy.BuildTestDAOFile(dbFactroy)

	factroy.BuildTestControllerFile(dbFactroy)

	factroy.BuildTestESServiceFile(dbFactroy)

	dir, f, c = factroy.BuildTestServiceFile(dbFactroy)
	dbFactroy.TestFiles = append(dbFactroy.TestFiles, f)

	return
}
