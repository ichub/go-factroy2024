package gofactroy

import (
	"container/list"
	"encoding/json"
	"fmt"
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"log"
	"strings"
	"time"
)

/*
   @Title    文件名称: JoinQueryFactroy.go
   @Description  描述: 代码工厂 JoinQueryFactroy 工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
const multiPb = "PbMulti"

type JoinQueryFactroy struct {
	//all table
	Tables []string //第一个为关联主表
	//单表信息
	Table, TableAlias, Pkey, PkeyType string

	Selects string
	//关联主表
	Joins    []string
	GoFields string //go dto GoFields

	Keys  map[string]string
	Alias map[string]string `json:"-"`

	dirFileCS []*metadata.DirFileC             `json:"-"`
	Mapcs     map[string](*[]metadata.Columns) `json:"-"`

	DbFactroy *dbfactroy.DbFactroy `json:"-"`
}

func (joinQuery *JoinQueryFactroy) AppendDfc(dir, f, c string) {

	joinQuery.dirFileCS = append(joinQuery.dirFileCS, &metadata.DirFileC{dir, f, c})

}

func (joinQuery *JoinQueryFactroy) SendEmail() *JoinQueryFactroy {

	subj := "生成MultiQuery代码：" + utils.Ip4() + " " + strings.Join(joinQuery.Tables, ",")

	var ca []string

	for _, v := range joinQuery.dirFileCS {
		ca = append(ca, v.C)
	}
	c := strings.Join(ca, "<br/")
	c = strings.ReplaceAll(c, "\n", "<br/>")
	c = strings.ReplaceAll(c, "\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
	c = strings.ReplaceAll(c, " ", "&nbsp;")

	joinQuery.DbFactroy.SendEmail(subj, c)
	return joinQuery
}

func (joinQuery *JoinQueryFactroy) LogDfc() *JoinQueryFactroy {
	log.Println(joinQuery.ToString())

	fmt.Printf("end gofactroy joinquery %s...你产生的多表关联代码路径如下:\n", joinQuery.TableAlias)
	log.Println("end gofactroy joinquery...你产生的多表关联代码路径如下 ： ")

	for _, v := range joinQuery.dirFileCS {
		fmt.Println(v.Dir, v.File)
		log.Println(v.Dir, v.File)
	}
	log.Println("gofactroy joinquery...你产生的多表关联代码！")
	fmt.Println("gofactroy joinquery...你产生的多表关联代码！")

	return joinQuery
}

/*
	tables "a,b" employee.id,employee.name
*/
func (joinQuery *JoinQueryFactroy) Ini(dbFactroy *dbfactroy.DbFactroy, tables string) *JoinQueryFactroy {
	joinQuery.Keys = make(map[string]string)
	joinQuery.Alias = make(map[string]string)

	joinQuery.Tables = strings.Split(tables, ",")

	joinQuery.Table = strings.Trim(joinQuery.Tables[0], " ")
	joinQuery.TableAlias = joinQuery.Table
	joinQuery.DbFactroy = dbFactroy
	dbFactroy.Table = joinQuery.Table

	for _, v := range joinQuery.Tables {
		joinQuery.Alias[v] = v
	}
	return joinQuery.CheckTables()
}

func (joinQuery *JoinQueryFactroy) CheckTables() *JoinQueryFactroy {
	for _, t := range joinQuery.Tables {
		joinQuery.DbFactroy.Table = t
		b := joinQuery.DbFactroy.CheckTableExist()
		if !b {
			panic(t + "表不存在！")
		}
	}
	return joinQuery
}

func (joinQuery *JoinQueryFactroy) SelectFields(selectFields string) *JoinQueryFactroy {

	joinQuery.Selects = strings.Trim(selectFields, " ")
	return joinQuery
}

func (joinQuery *JoinQueryFactroy) CheckKeys(dbFactroy *dbfactroy.DbFactroy) {
	if len(joinQuery.Mapcs) == 0 {
		joinQuery.Mapcs = joinQuery.findColumns(dbFactroy)
	}
	for k, v := range joinQuery.Keys {
		k = strings.Trim(k, " ")
		v = strings.Trim(v, " ")
		c := joinQuery.checkExist(joinQuery.Mapcs, v)
		if c == nil {
			panic(v + "关联key不存在！")
		}
		c = joinQuery.checkExist(joinQuery.Mapcs, k)
		if c == nil {
			panic(k + "关联key不存在！")
		}
	}
}

//db := dbinst.Table("employee").Select("employee.id,employee.name")
//db = db.Joins("JOIN department on department.id = employee.departmentId")
//db = db.Order("id desc ").Limit(5).Find(&ms)
func (joinQuery *JoinQueryFactroy) buildJoins(dbfactroy *dbfactroy.DbFactroy) {
	joinQuery.CheckKeys(dbfactroy)
	//JOIN department on department.id = employee.departmentId
	for key, value := range joinQuery.Keys {
		t := strings.Split(key, ".")
		if t[0] == joinQuery.Table {
			t = strings.Split(value, ".")
		}
		joinQuery.Joins = append(joinQuery.Joins, fmt.Sprintf("dbc = dbc.Joins(\"JOIN %s on %s = %s\") ", t[0], key, value))
	}

	return
}

/*
	指定生成结果转json字符串
*/
func (joinQuery *JoinQueryFactroy) String() string {
	s, err := json.Marshal(joinQuery)
	if err != nil {
		fmt.Println(err.Error())
	}
	return string(s)

}

func (joinQuery *JoinQueryFactroy) ToString() string {
	s, err := json.MarshalIndent(joinQuery, "", " ")
	if err != nil {
		fmt.Println(err.Error())
	}
	return string(s)

}

func (joinQuery *JoinQueryFactroy) checkExist(mapcs map[string](*[]metadata.Columns), fields string) *metadata.Columns {
	fields = strings.Trim(fields, " ")
	table_fields := strings.Split(fields, ".")
	cs := mapcs[strings.Trim(table_fields[0], " ")]
	if cs == nil {
		return nil
	}
	for _, v := range *cs {
		if v.ColumnName == table_fields[1] {
			return &v
		}
	}
	return nil
}
func (joinQuery *JoinQueryFactroy) MakeModelProtoBody(columns *[]metadata.Columns) *list.List {

	lst := list.New()
	i := 5
	for _, v := range *columns {
		// fmt.Println(v.String())
		dt := metadata.MetadataContextInst.FindProtoType(v.DataType)
		if dt == "" {
			dt = "string"
		}
		modelName := utils.Case2Camel(v.TableName)
		lst.PushBack(fmt.Sprintf("\t%s %s%s = %d;", dt, modelName, utils.Case2Camel(v.ColumnName), i))
		i++
	}
	for i := lst.Front(); i != nil; i = i.Next() {
		log.Println(i.Value)
	}
	return lst
}

/*
	selectS employee.id,employee.name
*/
func (joinQuery *JoinQueryFactroy) FilterCs(dbFactroy *dbfactroy.DbFactroy) (cs *[]metadata.Columns) {

	cs = &[]metadata.Columns{}

	mapcs := joinQuery.findColumns(dbFactroy)
	for _, v := range strings.Split(joinQuery.Selects, ",") {
		v = strings.Trim(v, " ")
		log.Println(strings.Split(v, "."))
		c := joinQuery.checkExist(mapcs, v)
		if c == nil {
			panic(v + "不存在！")
		} else {
			*cs = append(*cs, *c)
		}
	}
	return
}

func (joinQuery *JoinQueryFactroy) iniFields(columns *[]metadata.Columns) (lst *list.List) {

	s := `
	entity.{{.colname}} = new({{.goType}})
	`

	lst = list.New()
	for _, v := range *columns {
		colname := utils.Case2Camel(v.TableName + "_" + v.ColumnName)
		goType := v.FindGoType(v.DataType)

		vars := make(map[string]interface{})
		vars["goType"] = goType
		vars["colname"] = colname
		lst.PushBack(utils.ParseTemplateString(s, vars))

	}

	return lst
}

func (joinQuery *JoinQueryFactroy) buildGoFields(dbFactroy *dbfactroy.DbFactroy, columns *[]metadata.Columns) {

	lines := list.New()

	for _, v := range *columns {
		col_comment := fmt.Sprintf("\t/*  %s  */", v.ColumnComment)
		lines.PushBack(col_comment)
		dt := dbFactroy.FindGoType(v.DataType)

		col_def := v.TableName + "_" + v.ColumnName + ";type:" + v.ColumnType
		if v.ColumnName == dbFactroy.Pkey {
			col_def = col_def + ";PRIMARY_KEY"
		}
		if dbFactroy.IsMysql() {
			col_def = col_def + fmt.Sprintf(";comment:'%s'", v.ColumnComment)
		}

		json_def := v.TableName + "_" + v.ColumnName
		if dt == "int64" {
			json_def = json_def + ",string"
		}
		fieldName := utils.Case2Camel(v.TableName + "_" + v.ColumnName)
		lines.PushBack(fmt.Sprintf("\t%s *%s `gorm:\"column:%s\" json:\"%s\"`",
			fieldName, dt, col_def, json_def))
	}

	for i := lines.Front(); i != nil; i = i.Next() {
		log.Println(i.Value)
	}
	joinQuery.GoFields = utils.List2string(lines)
}

func (joinQuery *JoinQueryFactroy) buildSelects() string {
	fields := []string{}
	for _, v := range strings.Split(joinQuery.Selects, ",") {
		v = strings.Trim(v, " ")
		fs := strings.Split(v, ".")
		v := v + " as " + strings.Join(fs, "_")
		fields = append(fields, v)

	}
	return strings.Join(fields, ",")

}

//timeStr := v.Birthday.Format("2006-01-02 15:04:05")
//   inst.MapJson2Field[] = ""   {{.MapJson2Field}}

func (joinQuery *JoinQueryFactroy) MapJson2Field(columns *[]metadata.Columns) (lst *list.List) {
	s := `
	inst.MapJson2Field["{{.JsonField}}"] = "{{.TableField}}"`

	lst = list.New()
	for _, v := range *columns {

		vars := make(map[string]interface{})
		vars["JsonField"] = v.TableName + "_" + v.ColumnName
		vars["TableField"] = v.TableName + "." + v.ColumnName

		map2fields := utils.ParseTemplateString(s, vars)
		lst.PushBack(map2fields)
	}

	return
}

func (joinQuery *JoinQueryFactroy) BuildIf(columns *[]metadata.Columns) (lst *list.List) {

	lst = list.New()
	for _, column := range *columns {
		stat := InstDaofactroy.FindStat(column)

		vars := make(map[string]interface{})
		vars["table_name"] = column.TableName
		vars["field_name"] = column.ColumnName
		vars["goFieldName"] = utils.Case2Camel(column.TableName + "_" + column.ColumnName)

		wheres := utils.ParseTemplateString(stat, vars)
		lst.PushBack(wheres)
	}

	return
}

func (joinQuery *JoinQueryFactroy) FindPgPkey(dbfactroy *dbfactroy.DbFactroy) {
	pkeys := dbfactroy.FindPgPkey(joinQuery.Tables[0])
	if len(pkeys) > 0 {
		dbfactroy.Pkey = pkeys[0].ColName
		dbfactroy.PkeyType = pkeys[0].TypeName
	} else {
		dbfactroy.Pkey = "id"
		dbfactroy.PkeyType = "int64"
	}
}

func (joinQuery *JoinQueryFactroy) fbase() string {

	return "joinquery" + utils.Case2Camel(joinQuery.TableAlias)
}

func (joinQuery *JoinQueryFactroy) findColumns(dbfactroy *dbfactroy.DbFactroy) (mapcs map[string](*[]metadata.Columns)) {
	mapcs = make(map[string](*[]metadata.Columns))
	i := 1
	for _, table := range joinQuery.Tables {
		dbfactroy.Table = table
		mapcs[table] = dbfactroy.FindColumns()
		if i == 1 {
			joinQuery.Pkey = dbfactroy.Pkey
			joinQuery.PkeyType = dbfactroy.PkeyType
		}
		i++
	}
	if !dbfactroy.IsMysql() {
		joinQuery.FindPgPkey(dbfactroy)
		joinQuery.Pkey = dbfactroy.Pkey
		joinQuery.PkeyType = dbfactroy.PkeyType
	}
	// fmt.Println(joinQuery)
	dbfactroy.Pkey = joinQuery.Pkey
	dbfactroy.PkeyType = joinQuery.PkeyType

	return
}

//找各表字段定义
//
func (joinQuery *JoinQueryFactroy) buildGetSet(cs *metadata.Columns) string {
	s := `
	func (entity *{{.ModelPath}}MultiQryDTO) Get{{.FieldName}}() {{.goType}} {
		if entity.{{.FieldName}} == nil {
			{{.ReturnValue}}
		}
		return * entity.{{.FieldName}}		
		
	}

	func (entity *{{.ModelPath}}MultiQryDTO) Set{{.FieldName}}({{.FieldName}} {{.goType}}) {
		entity.{{.FieldName}}= &{{.FieldName}}
	}
`
	vars := make(map[string]interface{})

	vars["ModelName"] = utils.UcfirstCase2Camel(joinQuery.TableAlias)
	vars["FieldName"] = utils.Case2Camel(cs.TableName + "_" + cs.ColumnName)
	vars["goType"] = cs.FindGoType(cs.DataType)

	vars["ReturnValue"] = cs.ReturnValue()

	return utils.ParseTemplateString(s, vars)
}

func (joinQuery *JoinQueryFactroy) buildGetSets(cs *[]metadata.Columns) string {
	var r []string
	for _, v := range *cs {
		r = append(r, joinQuery.buildGetSet(&v))
	}
	return strings.Join(r, "\n")

}
func (joinQuery *JoinQueryFactroy) buildProtoFile(tool *dbfactroy.DbFactroy) (dir, f, c string) {

	cs := joinQuery.FilterCs(tool)
	ps := joinQuery.MakeModelProtoBody(cs)

	fbase := joinQuery.fbase()
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirProto, fbase)

	f, c = fbase+"Proto.proto", joinQuery.buildProto(tool, ps)

	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)
	filefactroy.FileFactroyInst.WriteFileModule(filefactroy.FileFactroyInst.GendirPbTool, f, c)
	joinQuery.AppendDfc(dir, f, c)

	return
}

//ReadTemplateServiceInterface
func (joinQuery *JoinQueryFactroy) buildProto(dbfactroy *dbfactroy.DbFactroy, ps *list.List) string {

	rb := filefactroy.FileFactroyInst.ReadTemplateMqProto()

	vars := make(map[string]interface{})
	vars["Module"] = utils.Case2Camel(joinQuery.TableAlias) + multiPb
	vars["LcModule"] = strings.ToLower(utils.Case2Camel(joinQuery.TableAlias)) + multiPb
	vars["ModuleMessage"] = utils.List2stringBy(ps, "\n\t")
	vars["pkey"] = utils.Lcfirst(utils.Case2Camel(dbfactroy.Pkey))
	vars["pkeyType"] = dbfactroy.FindProtoType(dbfactroy.PkeyType)

	return utils.ParseTemplateString(string(rb), vars)

}

func (joinQuery *JoinQueryFactroy) buildDto(dbfactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateMqDTO()
	cs := joinQuery.FilterCs(dbfactroy)

	joinQuery.buildGoFields(dbfactroy, cs)

	lines := joinQuery.iniFields(cs)

	vars := make(map[string]interface{})
	vars["ModelName"] = utils.Case2Camel(joinQuery.TableAlias)
	vars["buildGoFields"] = joinQuery.GoFields
	vars["iniFields"] = utils.List2string(lines)
	vars["GetSets"] = joinQuery.buildGetSets(cs)

	vars["FileName"] = "joinquery" + utils.Case2Camel(joinQuery.Table) + "Dto.go"
	vars["Description"] = "DTO" + utils.Case2Camel(joinQuery.Table) + "Dto"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)
	vars["ModelNames"] = strings.Join(joinQuery.Tables, ",")

	return utils.ParseTemplateString(string(bs), vars)

}

func (joinQuery *JoinQueryFactroy) buildDtoFile(tool *dbfactroy.DbFactroy) (dir string, f string, c string) {
	fbase := joinQuery.fbase()
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirDto, fbase)

	f, c = fbase+"Dto.go", joinQuery.buildDto(tool)

	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)
	joinQuery.AppendDfc(dir, f, c)

	return
}

func (joinQuery *JoinQueryFactroy) buildDao(dbfactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateMqDAO()

	vars := make(map[string]interface{})
	vars["FileName"] = "joinquery" + utils.Case2Camel(joinQuery.Table) + "Dao.go"
	vars["Description"] = "DAO" + utils.Case2Camel(joinQuery.Table) + "Dao"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(joinQuery.TableAlias)

	vars["Table"] = joinQuery.Table
	vars["Selects"] = joinQuery.buildSelects()

	joinQuery.buildJoins(dbfactroy)
	vars["Joins"] = strings.Join(joinQuery.Joins, "\n\t")

	cs := joinQuery.FilterCs(dbfactroy)

	vars["BuildWheres"] = utils.List2string(joinQuery.BuildIf(cs))
	vars["MapJson2Field"] = utils.List2string(joinQuery.MapJson2Field(cs))

	return utils.ParseTemplateString(string(bs), vars)

}

func (joinQuery *JoinQueryFactroy) buildDaoFile(tool *dbfactroy.DbFactroy) (dir string, f string, c string) {

	fbase := joinQuery.fbase()
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirDao, fbase)

	f, c = fbase+"Dao.go", joinQuery.buildDao(tool)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	joinQuery.AppendDfc(dir, f, c)

	return
}

func (joinQuery *JoinQueryFactroy) buildService(dbfactroy *dbfactroy.DbFactroy, buildEs bool) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateMqService()

	vars := make(map[string]interface{})

	vars["FileName"] = "joinquery" + utils.Case2Camel(joinQuery.Table) + "Service.go"
	vars["Description"] = "服务multiquery" + utils.Case2Camel(joinQuery.Table) + "Service"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)
	vars["queryTable"] = joinQuery.Table

	vars["ModelName"] = utils.Case2Camel(joinQuery.TableAlias)
	vars["ESModelName"] = utils.Case2Camel(joinQuery.TableAlias)
	if buildEs {
		vars["ESModelName"] = "ES" + vars["ESModelName"].(string)
	}

	vars["pkeyField"] = dbfactroy.Pkey
	vars["pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["pkeyType"] = dbfactroy.FindProtoType(dbfactroy.PkeyType)
	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbfactroy.FindGoType(dbfactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	}
	s := string(bs)
	if !buildEs {
		s = strings.Replace(s, "esservice", "service", 1)
	}
	return utils.ParseTemplateString(s, vars)

}

func (joinQuery *JoinQueryFactroy) BuildServiceFile(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {
	fbase := joinQuery.fbase()
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirService, fbase)

	f, c = fbase+"Service.go", joinQuery.buildService(dbfactroy, false)

	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)
	joinQuery.AppendDfc(dir, f, c)

	return
}

func (joinQuery *JoinQueryFactroy) buildTestService(dbfactroy *dbfactroy.DbFactroy, buildEs bool) string {
	var bs interface{}
	if buildEs {
		bs = filefactroy.FileFactroyInst.ReadTestTemplateMqESSrv()
	} else {
		bs = filefactroy.FileFactroyInst.ReadTestTemplateMqSrv()
	}

	vars := make(map[string]interface{})

	vars["FileName"] = "Mq_" + utils.Case2Camel(joinQuery.Table) + "Srv_test.go"
	vars["Description"] = "测试服务Mq_" + utils.Case2Camel(joinQuery.Table) + "Srv"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["queryTable"] = joinQuery.Table
	vars["ModelName"] = utils.Case2Camel(joinQuery.TableAlias)
	vars["ESModelName"] = utils.Case2Camel(joinQuery.TableAlias)
	if buildEs {
		vars["ESModelName"] = "ES" + vars["ESModelName"].(string)
	}
	vars["pkeyField"] = dbfactroy.Pkey
	vars["pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["pkeyType"] = dbfactroy.FindProtoType(dbfactroy.PkeyType)

	vars["StringFieldName"] = utils.Case2Camel(dbfactroy.StringFieldName)
	vars["StringFieldLen"] = dbfactroy.StringFieldLen

	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbfactroy.FindGoType(dbfactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	} else if dbfactroy.FindGoType(dbfactroy.PkeyType) == "model.BitField" {
		vars["PkeyValue"] = "false"
		vars["fmtType"] = "%s"
	}

	s := string(bs.([]byte))
	if !buildEs {
		s = strings.ReplaceAll(s, "esservice", "service")
	}
	return utils.ParseTemplateString(s, vars)

}

func (joinQuery *JoinQueryFactroy) BuildTestServiceFile(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := "joinquery" + utils.Case2Camel(joinQuery.TableAlias)
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirServiceTest, fbase)

	f, c = fbase+"Srv_test.go", joinQuery.buildTestService(dbfactroy, false)

	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)
	joinQuery.AppendDfc(dir, f, c)

	return dir, f, c
}

func (joinQuery *JoinQueryFactroy) Build(tool *dbfactroy.DbFactroy) (dir, f, c string) {

	fmt.Println("start gofactroy joinquery...")

	dir, f, c = joinQuery.buildDtoFile(tool)
	fmt.Println("finish gofactroy joinquery dto:" + dir + " " + f)
	fmt.Println("start gofactroy Proto...")

	dir, f, c = joinQuery.buildProtoFile(tool)

	fmt.Println("finish gofactroy joinquery Proto:" + dir + " " + f)

	dir, f, c = joinQuery.buildDaoFile(tool)
	fmt.Println("finish gofactroy joinquery dao:" + dir + " " + f)

	dir, f, c = joinQuery.BuildServiceFile(tool)
	fmt.Println("finish gofactroy joinquery service: " + dir + " " + f + "!")

	dir, f, c = joinQuery.BuildTestServiceFile(tool)
	fmt.Println("finish gofactroy testServiceFile: " + dir + " " + f + "!")

	joinQuery.LogDfc()
	joinQuery.SendEmail()

	return
}

func (joinQuery *JoinQueryFactroy) Demo(tool *dbfactroy.DbFactroy) {

	joinQuery.Ini(tool, "cms_column,cms_content")
	joinQuery.SelectFields("cms_column.column_id,cms_content.id")
	joinQuery.Keys["cms_column.column_id"] = "cms_content.id"

	joinQuery.Build(tool)

}
