package gofactroy

import (
	"container/list"
	"fmt"
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"strings"
	"time"
)

/*
   @Title    文件名称: ModelFactroy.go
   @Description  描述: 代码工厂 ModelFactroy 工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type ModelFactroy struct {
	rpcfactroy RpcFactroy
}

func (model *ModelFactroy) makeModelIniLst(columns *[]metadata.Columns, pkey string, iniNil bool) *list.List {

	s := `
	//entity.{{.colname}} = new({{.goType}})
	`
	if iniNil {
		s = `
		//if entity.{{.colname}} == nil {
			//entity.{{.colname}} = new({{.goType}})
		//}
	`
	}
	lst := list.New()
	for _, v := range *columns {
		colname := utils.Case2Camel(v.ColumnName)

		goType := v.FindGoType(v.DataType)
		if goType == "" {
			goType = "string"
		}

		if v.ColumnName != pkey {

			vars := make(map[string]interface{})
			vars["goType"] = goType
			vars["colname"] = colname
			lst.PushBack(utils.ParseTemplateString(s, vars))
		}

	}

	utils.LogList(lst)
	return lst
}

func (model *ModelFactroy) buildComment(table string, columns *[]metadata.Columns) *[]string {
	var rets []string
	for _, v := range *columns {
		s := fmt.Sprintf(`comment on column %s.%s is '%s';`, table, v.ColumnName, v.ColumnComment)
		rets = append(rets, s)
	}
	return &rets
}

func (model *ModelFactroy) makeM2PLst(columns *[]metadata.Columns) *list.List {

	s := `{{.Tab}}cpentity.{{.colname}} =  *entity.{{.colname}}`

	lst := list.New()
	for _, v := range *columns {
		colname := utils.Case2Camel(v.ColumnName)
		//goType := dbfactroy.FindGoType(v.DataType)
		vars := make(map[string]interface{})
		vars["colname"] = colname
		vars["Tab"] = fmt.Sprintf("\t\t")
		lst.PushBack(utils.ParseTemplateString(s, vars))
	}

	utils.PrintList(lst)
	return lst
}
func (model *ModelFactroy) makeM2MLst(dbfactroy *dbfactroy.DbFactroy, columns *[]metadata.Columns) *list.List {

	s := `{{.Tab}}entity.{{.colname}} =  cpentity.{{.colname}}`

	lst := list.New()
	for _, v := range *columns {
		if dbfactroy.Pkey == v.ColumnName {
			continue
		}
		colname := utils.Case2Camel(v.ColumnName)
		vars := make(map[string]interface{})
		vars["Tab"] = fmt.Sprintf("\t\t")
		vars["colname"] = colname
		lst.PushBack(utils.ParseTemplateString(s, vars))
	}

	utils.LogList(lst)
	return lst
}
func (model *ModelFactroy) makeM2MNotNilLst(dbfactroy *dbfactroy.DbFactroy, columns *[]metadata.Columns) *list.List {

	s := `{{.Tab}}entity.{{.colname}} = cpentity.{{.colname}}`

	lst := list.New()
	for _, v := range *columns {
		if dbfactroy.Pkey == v.ColumnName {
			continue
		}
		colname := utils.Case2Camel(v.ColumnName)
		vars := make(map[string]interface{})
		vars["Tab"] = fmt.Sprintf("\t\t")
		vars["colname"] = colname
		lst.PushBack(utils.ParseTemplateString(s, vars))
	}

	utils.LogList(lst)
	return lst
}

func (model *ModelFactroy) buildGetSet(cs *metadata.Columns) string {
	s := `
	func (entity *{{.Model}}) Get{{.FieldName}}() {{.goType}} {
		 
		return  entity.{{.FieldName}}		
		
	}

	func (entity *{{.Model}}) Set{{.FieldName}}({{.FieldName}} {{.goType}}) {
		entity.{{.FieldName}}= {{.FieldName}}
	}

`
	vars := make(map[string]interface{})

	t := utils.UcfirstCase2Camel(cs.TableName)
	vars["Model"] = t
	vars["FieldName"] = utils.Case2Camel(cs.ColumnName)
	vars["goType"] = cs.FindGoType(cs.DataType)
	vars["ReturnValue"] = cs.ReturnValue()

	return utils.ParseTemplateString(s, vars)
}

func (model *ModelFactroy) buildGetSets(cs *[]metadata.Columns) string {
	var r []string
	for _, v := range *cs {
		r = append(r, model.buildGetSet(&v))
	}
	return strings.Join(r, "\n")

}

func (model *ModelFactroy) buildModel(dbfactroy *dbfactroy.DbFactroy,
	columns *[]metadata.Columns,
	ms *list.List,
	msdto *list.List) string {
	bs := filefactroy.FileFactroyInst.ReadTemplateModel()

	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbfactroy.Table) + ".go"
	vars["Description"] = "实体" + utils.Case2Camel(dbfactroy.Table) + ""
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["Model"] = utils.Case2Camel(dbfactroy.Table)
	vars["ModelDefine"] = utils.List2string(ms)
	vars["ModelDefineDto"] = utils.List2string(msdto)

	vars["Pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["PkeyType"] = dbfactroy.FindGoType(dbfactroy.PkeyType)
	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbfactroy.FindGoType(dbfactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	}

	vars["Table"] = dbfactroy.Table
	vars["TableComment"] = dbfactroy.TableComment

	vars["IfComment"] = "//"
	vars["sql"] = "``"
	if !dbfactroy.IsMysql() {
		vars["IfComment"] = ""
		vars["sql"] = "`" + strings.Join(*model.buildComment(dbfactroy.Table, columns), "\n\t\t") + "`"
	}

	vars["Model2PbMsg"] = utils.List2string(model.rpcfactroy.Model2PbMsg(columns))
	vars["PbMsg2Model"] = utils.List2string(model.rpcfactroy.PbMsg2Model(columns))
	vars["IniPbMsg"] = utils.List2string(model.rpcfactroy.IniPbMsg(columns))

	vars["copy"] = utils.List2string(model.makeM2MLst(dbfactroy, columns))
	vars["copyNotNil"] = utils.List2string(model.makeM2MNotNilLst(dbfactroy, columns))

	iniList := model.makeModelIniLst(columns, dbfactroy.Pkey, false)
	iniNilList := model.makeModelIniLst(columns, dbfactroy.Pkey, true)
	vars["iniFields"] = utils.List2stringBy(iniList, "\t\t")
	vars["iniNilFields"] = utils.List2stringBy(iniNilList, "\t\t")

	vars["buildGetSets"] = model.buildGetSets(columns)

	return utils.ParseTemplateString(string(bs), vars)

}

func (model *ModelFactroy) buildModelFile(dbFactroy *dbfactroy.DbFactroy,
	cs *[]metadata.Columns, ms *list.List, msdto *list.List) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirModel, fbase)

	f, c = fbase+".go", model.buildModel(dbFactroy, cs, ms, msdto)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}

func (model *ModelFactroy) buildProtoFile(dbFactroy *dbfactroy.DbFactroy, ps *list.List) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirProto, fbase)

	f, c = fbase+"Proto.proto", model.buildProto(dbFactroy, ps)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)
	filefactroy.FileFactroyInst.WriteFileModule(filefactroy.FileFactroyInst.GendirPbTool, f, c)

	return
}

//ReadTemplateServiceInterface
func (model *ModelFactroy) buildProto(dbfactroy *dbfactroy.DbFactroy, ps *list.List) string {

	b := filefactroy.FileFactroyInst.ReadTemplateProto()
	vars := make(map[string]interface{})
	vars["Module"] = utils.Case2Camel(dbfactroy.Table)
	vars["LcModule"] = strings.ToLower(utils.Case2Camel(dbfactroy.Table))
	vars["ModuleMessage"] = utils.List2stringBy(ps, "\n\t")
	vars["pkey"] = utils.Lcfirst(utils.Case2Camel(dbfactroy.Pkey))
	vars["pkeyType"] = dbfactroy.FindProtoType(dbfactroy.PkeyType)

	return utils.ParseTemplateString(string(b), vars)

}

func (model *ModelFactroy) Build(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	cs, ms, ps, msdto := dbFactroy.BuildModel()
	dir, f, c = model.buildProtoFile(dbFactroy, ps)
	dir, f, c = model.buildModelFile(dbFactroy, cs, ms, msdto)

	return
}
