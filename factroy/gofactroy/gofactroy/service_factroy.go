package gofactroy

import (
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"strings"
	"time"
)

/*
   @Title    文件名称: ServiceFactroy.go
   @Description  描述: 代码工厂 ServiceFactroy 工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type ServiceFactroy struct {
}

//ReadTemplateServiceInterface
func (factroy *ServiceFactroy) buildServiceInterface(dbfactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateServiceInterface()

	vars := make(map[string]interface{})
	vars["FileName"] = utils.Case2Camel(dbfactroy.Table) + "Interface.go"
	vars["Description"] = "接口" + utils.Case2Camel(dbfactroy.Table) + "Interface"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbfactroy.Table)
	vars["pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["pkeyType"] = dbfactroy.FindGoType(dbfactroy.PkeyType)

	return utils.ParseTemplateString(string(bs), vars)

}

func (factroy *ServiceFactroy) BuildESServiceFile(dbfactroy *dbfactroy.DbFactroy) (d, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbfactroy.Table))
	d = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirEsservice, fbase)

	f, c = fbase+"ESService.go", factroy.buildService(dbfactroy, true)
	filefactroy.FileFactroyInst.WriteFileModule(d, f, c)

	return
}

func (factroy *ServiceFactroy) BuildInterfaceFile(dbFactroy *dbfactroy.DbFactroy) (sdir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	sdir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirInterface, fbase)

	f, c = fbase+"Interface.go", factroy.buildServiceInterface(dbFactroy)
	filefactroy.FileFactroyInst.WriteFileModule(sdir, f, c)

	return
}

func (factroy *ServiceFactroy) buildESService(dbfactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateService()

	vars := make(map[string]interface{})
	vars["ModelName"] = "ES" + utils.Case2Camel(dbfactroy.Table)
	vars["pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["pkeyType"] = dbfactroy.FindProtoType(dbfactroy.PkeyType)
	s := string(bs)
	s = strings.Replace(s, "service", "esservice", 0)

	return utils.ParseTemplateString(s, vars)

}

func (factroy *ServiceFactroy) buildService(dbFactroy *dbfactroy.DbFactroy, buildEs bool) string {
	bs := filefactroy.FileFactroyInst.ReadTemplateService()

	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Service.go"
	vars["Description"] = "服务" + utils.Case2Camel(dbFactroy.Table) + "Service"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbFactroy.Table)
	vars["ESModelName"] = utils.Case2Camel(dbFactroy.Table)
	if buildEs {
		vars["ESModelName"] = "ES" + vars["ESModelName"].(string)
	}
	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindGoType(dbFactroy.PkeyType)
	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbFactroy.FindGoType(dbFactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	}
	s := string(bs)
	if !buildEs {
		s = strings.Replace(s, "esservice", "service", 1)
	}
	return utils.ParseTemplateString(s, vars)

}

func (factroy *ServiceFactroy) BuildServiceFile(dbFactroy *dbfactroy.DbFactroy) (d, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	d = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirService, fbase)

	f, c = fbase+"Service.go", factroy.buildService(dbFactroy, false)
	filefactroy.FileFactroyInst.WriteFileModule(d, f, c)

	return d, f, c
}

func (factroy *ServiceFactroy) Build(dbFactroy *dbfactroy.DbFactroy) (d, f, c string) {

	factroy.BuildInterfaceFile(dbFactroy)
	factroy.BuildESServiceFile(dbFactroy)

	return factroy.BuildServiceFile(dbFactroy)

}
