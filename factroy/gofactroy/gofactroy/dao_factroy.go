package gofactroy

import (
	"container/list"
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"time"
)

/*
   @Title    文件名称: DaoFactroy.go
   @Description  描述: 代码工厂 DaoFactroy 工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type DaoFactroy struct {
	mapstat map[string]string
}

var InstDaofactroy DaoFactroy

func init() {
	InstDaofactroy.mapstat = make(map[string]string)

	InstDaofactroy.mapstat["s_default"] = `
	if pp.{{.goFieldName}} != "" {
		b,_ := strconv.ParseBool(pp.{{.goFieldName}}) //ParseBool(str string)(value bool, err error)
		dbc = dbc.Where("{{.field_name}}=?", b)
	}
`

	InstDaofactroy.mapstat["s_numeric"] = `
	if pp.{{.goFieldName}} != 0 {
		dbc = dbc.Where("{{.field_name}}=?", pp.{{.goFieldName}})
	}`

	InstDaofactroy.mapstat["s_string"] = `
	if pp.{{.goFieldName}} != "" {
		
		if  param.FuzzyQuery { 
			dbc = dbc.Where("{{.field_name}} like ?",  "%"+pp.{{.goFieldName}}+"%")
		} else{
			dbc = dbc.Where("{{.field_name}}=?", pp.{{.goFieldName}})
		}

	}`

	InstDaofactroy.mapstat["s_datetimeUTC"] = `
	if  !pp.{{.goFieldName}}.IsZero() && pp.{{.goFieldName}}.Unix() != 0 {
		dbc = dbc.Where("{{.field_name}}=?", daoInst.localTimeUTCFormat( pp.{{.goFieldName}}.Time ))
	}`
	InstDaofactroy.mapstat["s_datetime"] = `
	if  !pp.{{.goFieldName}}.IsZero() && pp.{{.goFieldName}}.Unix() != 0 {
		dbc = dbc.Where("{{.field_name}}=?", daoInst.localTimeFormat( pp.{{.goFieldName}}.Time ))
	}`
	// && !pp.Adate.IsZero()
	InstDaofactroy.mapstat["s_date"] = `
	if !pp.{{.goFieldName}}.IsZero() && pp.{{.goFieldName}}.Unix() != 0 {
		dbc = dbc.Where("{{.field_name}}=?", daoInst.localDateFormat( pp.{{.goFieldName}} ))
	}`

}

func (factry *DaoFactroy) FindStat(c metadata.Columns) (s string) {
	s = InstDaofactroy.mapstat["s_default"]
	if c.IfNumeric() {
		s = InstDaofactroy.mapstat["s_numeric"]
	} else if c.IfString() {
		s = InstDaofactroy.mapstat["s_string"]
	} else if c.IfLocalDateInt() {
		s = InstDaofactroy.mapstat["s_date"]
	} else if c.IfLocalTimeInt() {
		s = InstDaofactroy.mapstat["s_datetime"]
	} else if c.IfLocalTimeUTCInt() {
		s = InstDaofactroy.mapstat["s_datetimeUTC"]
	}
	return
}

func (factry *DaoFactroy) buildIf(columns *[]metadata.Columns) (lst *list.List) {

	lst = list.New()
	for _, v := range *columns {
		stat := factry.FindStat(v)

		vars := make(map[string]interface{}) //vars["table_name"] = v.TableName
		vars["field_name"] = v.ColumnName
		vars["goFieldName"] = utils.Case2Camel(v.ColumnName)

		wheres := utils.ParseTemplateString(stat, vars)
		lst.PushBack(wheres)
	}

	return
}

func (factry *DaoFactroy) buildDAO(dbfactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateDAO()
	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbfactroy.Table) + "Dao.go"
	vars["Description"] = "DAO层" + utils.Case2Camel(dbfactroy.Table) + "Dao"
	vars["Author"] = dbfactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbfactroy.Table)
	vars["TableName"] = dbfactroy.Table
	vars["pkeyField"] = dbfactroy.Pkey
	vars["pkey"] = utils.Case2Camel(dbfactroy.Pkey)
	vars["pkeyType"] = dbfactroy.FindGoType(dbfactroy.PkeyType)

	cs := dbfactroy.FindColumns()
	vars["BuildWheres"] = utils.List2string(factry.buildIf(cs))

	return utils.ParseTemplateString(string(bs), vars)

}

func (factry *DaoFactroy) BuildDAOFile(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbfactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirDao, fbase)

	f, c = fbase+"Dao.go", factry.buildDAO(dbfactroy)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}

func (factry *DaoFactroy) Build(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {
	return factry.BuildDAOFile(dbfactroy)
}
