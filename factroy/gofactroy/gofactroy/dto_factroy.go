package gofactroy

import (
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"time"
)

/*
   @Title    文件名称: DtoFactroy.go
   @Description  描述: 代码工厂 DtoFactroy 工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type DtoFactroy struct {
}

//ReadTemplateServiceInterface
func (dto *DtoFactroy) buildDto(dbFactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateDto()

	vars := make(map[string]interface{})
	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Dto.go"
	vars["Description"] = "接口" + utils.Case2Camel(dbFactroy.Table) + "Dto"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["Model"] = utils.Case2Camel(dbFactroy.Table)
	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindProtoType(dbFactroy.PkeyType)

	return utils.ParseTemplateString(string(bs), vars)

}

func (dto *DtoFactroy) Build(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirDto, fbase)

	f, c = fbase+"Dto.go", dto.buildDto(dbFactroy)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}
