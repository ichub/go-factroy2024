package gofactroy

import (
	"gitee.com/ichub/go-factroy2024/common/base"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"strings"
	"time"
)

/*
   @Title    文件名称: ControllerFactroy.go
   @Description  描述: 代码工厂ControllerFactroy工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type ControllerFactroy struct {
	rpcfactroy RpcFactroy
}

func (factroy *ControllerFactroy) BuildController(dbFactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateController()

	vars := make(map[string]interface{})

	vars["FileName"] = utils.Lcfirst(utils.Case2Camel(dbFactroy.Table)) + "Controller.go"

	vars["Description"] = "服务" + utils.Case2Camel(dbFactroy.Table) + "Controller"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["LcModel"] = utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	vars["Model"] = utils.Case2Camel(dbFactroy.Table)
	vars["ESModelName"] = utils.Case2Camel(dbFactroy.Table)

	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindGoType(dbFactroy.PkeyType)
	vars["PkeyValue"] = "0"
	vars["fmtType"] = "%d"
	if dbFactroy.FindGoType(dbFactroy.PkeyType) == "string" {
		vars["PkeyValue"] = "\"\""
		vars["fmtType"] = "%s"
	}

	return utils.ParseTemplateString(string(bs), vars)

}

func (factroy *ControllerFactroy) BuildRpcController(dbFactroy *dbfactroy.DbFactroy) string {

	bs := filefactroy.FileFactroyInst.ReadTemplateRpcController()
	vars := make(map[string]interface{})

	vars["FileName"] = utils.Case2Camel(dbFactroy.Table) + "Controller.go"
	vars["Description"] = "Controller层" + utils.Case2Camel(dbFactroy.Table) + "Controller"
	vars["Author"] = dbFactroy.Author
	vars["DATETIME"] = time.Now().Format(base.FormatDateTime)

	vars["ModelName"] = utils.Case2Camel(dbFactroy.Table)
	vars["LcModelName"] = utils.Lcfirst(utils.Case2Camel(dbFactroy.Table))
	vars["TableName"] = dbFactroy.Table

	vars["pkeyField"] = dbFactroy.Pkey
	vars["pkey"] = utils.Case2Camel(dbFactroy.Pkey)
	vars["pkeyType"] = dbFactroy.FindProtoType(dbFactroy.PkeyType)
	pkeyType := dbFactroy.FindGoType(dbFactroy.PkeyType)
	if strings.Contains(pkeyType, "int64") {
		vars["Int64IdRemark"] = ""
		vars["Int32IdRemark"] = "// "
	} else {
		vars["Int64IdRemark"] = "// "
		vars["Int32IdRemark"] = ""
	}

	columns := dbFactroy.FindColumns()

	vars["Model2PbMsg"] = utils.List2string(factroy.rpcfactroy.Model2PbMsg(columns))
	vars["PbMsg2Model"] = utils.List2string(factroy.rpcfactroy.PbMsg2Model(columns))
	vars["IniPbMsg"] = utils.List2string(factroy.rpcfactroy.IniPbMsg(columns))

	vars["BuildWheres"] = utils.List2string(factroy.rpcfactroy.BuildIf(columns))
	return utils.ParseTemplateString(string(bs), vars)

}

func (factroy *ControllerFactroy) BuildControllerFile(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbfactroy.Table))
	dir = filefactroy.FileFactroyInst.TransDir(filefactroy.FileFactroyInst.GendirController, fbase)

	f, c = fbase+"Controller.go", factroy.BuildController(dbfactroy)
	filefactroy.FileFactroyInst.WriteFileModule(dir, f, c)

	return
}

func (factroy *ControllerFactroy) BuildControllerRpcFile(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fbase := utils.Lcfirst(utils.Case2Camel(dbfactroy.Table))

	dir = filefactroy.FileFactroyInst.GendirRpcController
	f, c = fbase+"RpcController.go", factroy.BuildRpcController(dbfactroy)
	filefactroy.FileFactroyInst.WriteFileModuleRpc(dir, f, c)

	dbfactroy.DirFileCS = append(dbfactroy.DirFileCS, metadata.DirFileC{dir, f, c})

	return

}

func (factroy *ControllerFactroy) Build(dbfactroy *dbfactroy.DbFactroy) (d, f, c string) {
	return factroy.BuildControllerFile(dbfactroy)

}
func (factroy *ControllerFactroy) BuildRpc(dbfactroy *dbfactroy.DbFactroy) (d, f, c string) {
	return factroy.BuildControllerRpcFile(dbfactroy)

}
