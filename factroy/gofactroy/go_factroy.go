package gofactroy

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/common/base/utils/email"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy/gofactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy/iface"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy/testcasefactroy"
	"log"
	"os"
	"strings"
)

/*
   @Title    文件名称: go_factroy.go
   @Description  描述: 代码工厂主文件

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type GoFactroy struct {
	dbConfig  *dbfactroy.DbConfig
	DbFactroy *dbfactroy.DbFactroy

	builderIfaces []iface.BuilderIface

	modelFactroy      gofactroy.ModelFactroy
	dtoFactroy        gofactroy.DtoFactroy
	daoFactroy        gofactroy.DaoFactroy
	serviceFactroy    gofactroy.ServiceFactroy
	rpcFactroy        gofactroy.RpcFactroy
	controllerFactroy gofactroy.ControllerFactroy

	testFactroy testcasefactroy.TestcaseFactroy
}

var buildFactroyInst *GoFactroy

func GetInst() *GoFactroy {
	return buildFactroyInst
}
func GetInstBy(dbFactroy *dbfactroy.DbFactroy) *GoFactroy {
	buildFactroyInst.DbFactroy = dbFactroy
	return buildFactroyInst
}
func init() {
	buildFactroyInst = &GoFactroy{}
	buildFactroyInst.Register()
}
func (buildFactoy *GoFactroy) Register() {
	if len(buildFactoy.builderIfaces) == 0 {

		buildFactoy.builderIfaces = []iface.BuilderIface{
			&gofactroy.ModelFactroy{},
			&gofactroy.DtoFactroy{},
			&gofactroy.DaoFactroy{},
			&gofactroy.ServiceFactroy{},
			&gofactroy.RpcFactroy{},
			&gofactroy.ControllerFactroy{},
		}

		fmt.Println("register builder...")
	}
}
func (buildFactoy *GoFactroy) IniDb(dbConfig *dbfactroy.DbConfig) (dbinst *gorm.DB) {
	buildFactoy.dbConfig = dbConfig
	return buildFactoy.Ini()
}

func (buildFactoy *GoFactroy) Ini() (dbinst *gorm.DB) {
	if buildFactoy.dbConfig == nil {

		buildFactoy.dbConfig = &dbfactroy.DbConfig{
			DbType:   common.DB_TYPE_MYSQL,
			DbName:   common.Module.DbName,
			User:     "root",
			Password: "leijmdas@163.comL",
			Host:     "huawei.akunlong.top",
			Port:     13306,
		}
	}
	buildFactoy.DbFactroy = &dbfactroy.DbFactroy{
		DbConfig:     buildFactoy.dbConfig,
		Author:       "leijianming@163.com",
		ModuleDefine: *common.Module,
	}

	dbinst = buildFactoy.DbFactroy.Ini()
	//dbinst = buildFactoy.DbInst
	//buildFactoy.buildFactroy = &GoFactroy{DbFactroy: buildFactoy.DbFactroy}
	buildFactoy.dbConfig.LogInfo()

	//var entity model.TableName1
	//entity.AutoMigrate(dbcontent.GetDB())

	// 多表关联查询
	//joinQuery(dbFactroy)
	//basetest.DemoDataSaveFindMysql()

	//imax := max(1, 2)	//fmt.Println(imax)
	return
}
func (buildFactoy *GoFactroy) BuildJoinQuery() {
	dbFactroy := buildFactoy.DbFactroy

	query := &gofactroy.JoinQueryFactroy{TableAlias: "EmployeeDpt"}
	query.Ini(dbFactroy, "employee,department,joblevel")
	query.SelectFields("employee.id,employee.name,employee.beginDate,department.name,joblevel.name")
	query.Keys["joblevel.id"] = "employee.jobLevelId"
	query.Keys["department.id"] = "employee.departmentId"

	query.Build(dbFactroy)

}

/*

   	rt:=reflect.TypeOf(tool)
   	fmt.Println(rt.FieldByName("Table"))


    c := exec.Command("cmd", "/C", "go fmt -w -s", build2.Gendir+"\\table_name1\\model")
    c.Run()
*/

//强胜，定时排期流程细化。 播放接口要想清楚。 广告类修改对已经排期的广告影响想想方案

//管理流程，业务流程

func (buildFactoy *GoFactroy) buildJoinQryEmp(dbFactroy *dbfactroy.DbFactroy) {
	/*
	   多表关联查询
	*/
	query := &gofactroy.JoinQueryFactroy{}
	query.Ini(dbFactroy, "employee,department,joblevel")
	query.SelectFields("employee.id,employee.name,employee.beginDate,department.name,joblevel.name")
	query.Keys["joblevel.id"] = "employee.jobLevelId"
	query.Keys["department.id"] = "employee.departmentId"
	// query.TableAlias = "employee_dpt"
	query.Build(dbFactroy)
	query.LogDfc()

}
func (buildFactoy *GoFactroy) IniLog() *GoFactroy {
	logFile, err := os.OpenFile(common.LOG_FILE, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println("open log service failed, err:", err)
		return buildFactoy
	}
	log.SetOutput(logFile)
	return buildFactoy
}
func (buildFactoy *GoFactroy) BuildTableDefault() (dir, f, c string) {
	return buildFactoy.BuildTable(buildFactoy.DbFactroy.Table)
}

/*
	单表生成
	ret key=filename value=content
*/
func (buildFactoy *GoFactroy) BuildTable(table string) (dir, f, c string) {

	buildFactoy.DbFactroy.Table = table
	dbFactroy := buildFactoy.DbFactroy
	dbFactroy.GenDir = filefactroy.FileFactroyInst.Gendir
	dbFactroy.GenDirTest = filefactroy.FileFactroyInst.GendirTest
	dbFactroy.FindTableComment()

	dbFactroy.Tables = append(dbFactroy.Tables, dbFactroy.Table)
	dir, f, c = buildFactoy.Build(dbFactroy)
	buildFactoy.testFactroy.Build(dbFactroy)

	dbFactroy.I++

	buildFactoy.dbConfig.LogInfo()
	dir, f, c = buildFactoy.builder(dbFactroy)

	return
}

func (buildFactoy *GoFactroy) builder(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {
	//buildFactoy.Register()

	for index, BuilderIface := range buildFactoy.builderIfaces {
		fmt.Println(index)
		BuilderIface.Build(dbFactroy)
	}
	fmt.Println("builder...")
	return
}

func (buildFactoy *GoFactroy) Build(dbFactroy *dbfactroy.DbFactroy) (dir, f, c string) {

	fmt.Printf("generate %d model %s:\n", dbFactroy.I+1, dbFactroy.Table)
	dir, f, c = buildFactoy.modelFactroy.Build(dbFactroy)

	fmt.Printf("generate dto %s:\n", dbFactroy.Table)
	buildFactoy.dtoFactroy.Build(dbFactroy)

	fmt.Printf("generate dao %s:\n", dbFactroy.Table)
	buildFactoy.daoFactroy.Build(dbFactroy)

	fmt.Printf("generate service %s:\n", dbFactroy.Table)
	buildFactoy.serviceFactroy.Build(dbFactroy)

	fmt.Printf("generate rpc %s:\n", dbFactroy.Table)
	dir, f, c = buildFactoy.rpcFactroy.Build(dbFactroy)

	fmt.Printf("generate controller %s:\n", dbFactroy.Table)
	buildFactoy.controllerFactroy.Build(dbFactroy)

	fmt.Printf("generate rpcController %s:\n", dbFactroy.Table)
	buildFactoy.controllerFactroy.BuildRpc(dbFactroy)

	fmt.Printf("generate testcode %d %s:\n\n", dbFactroy.I+1, dbFactroy.Table)
	return
}

func (buildFactoy *GoFactroy) SendEmail(subj, msg string) {
	emailReq := &email.SendRequest{
		Receivers: []string{"leijmdas_s180@163.com"},
		Subject:   subj,
		Message:   msg,
	}
	email.DefaultEmailHelper = &email.Client{}
	// retry send email
	err := email.DefaultEmailHelper.SendEmail(emailReq)
	if err != nil {
		fmt.Println(err.Error())
		panic(err.Error())
	}
}

func (buildFactoy *GoFactroy) BuildTables() (dir, f, c string) {
	var cs []string
	dbFactroy := buildFactoy.DbFactroy

	var tables []metadata.Tables = dbFactroy.FindTables()
	fmt.Println("表个数 ", len(tables), tables)
	for _, v := range tables {
		dbFactroy.Table = v.TableName
		dbFactroy.TableComment = &v.TableComment
		dir, f, c = buildFactoy.BuildTableDefault()
		cs = append(cs, c)
	}

	for _, v := range tables {
		fmt.Println(string(v.TableName) + "表转换完成！")
	}
	buildFactoy.BuildInfos()

	subj := "生成单表Go代码：" + utils.Ip4() + " " + dbFactroy.Table
	//msg := strings.Join(dbFactroy.TestFiles, "\n")
	cc := ""
	for _, v := range dbFactroy.DirFileCS {
		cc = cc + v.C
	}
	c = c + cc
	c = strings.ReplaceAll(c, "\n", "<br/>")
	c = strings.ReplaceAll(c, "\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
	c = strings.ReplaceAll(c, " ", "&nbsp;")

	buildFactoy.DbFactroy.SendEmail(subj, c)
	buildFactoy.dbConfig.LogInfo()
	return
}

func (buildFactoy *GoFactroy) BuildInfos() {
	tool := buildFactoy.DbFactroy
	tool.BuildInfo(filefactroy.FileFactroyInst.Gendir)
	tool.BuildInfoTest(filefactroy.FileFactroyInst.GendirTest)

}

func (buildFactoy *GoFactroy) BuildMultiQueryEmployee() {

	multiquery := &gofactroy.JoinQueryFactroy{}
	multiquery.Ini(buildFactoy.DbFactroy, "employee,department,joblevel")
	multiquery.SelectFields(`employee.beginDate,employee.id,employee.name,department.name`)
	multiquery.Keys["joblevel.id"] = "employee.jobLevelId"
	multiquery.Keys["department.id"] = "employee.departmentId"

	multiquery.Build(buildFactoy.DbFactroy)

}

func (buildFactoy *GoFactroy) BuildMqCmsContentColumn() {

	multiquery := &gofactroy.JoinQueryFactroy{}
	multiquery.Ini(buildFactoy.DbFactroy, "cms_content,cms_column")
	multiquery.SelectFields("cms_column.id,cms_content.content_id")
	multiquery.Keys["cms_column.id"] = "cms_content.content_id"
	multiquery.TableAlias = "CmsContentColumn"
	multiquery.Build(buildFactoy.DbFactroy)
}

func (buildFactoy *GoFactroy) buildEmpMulti() {
	/*
	   多表关联查询
	*/
	multiQuery := &gofactroy.JoinQueryFactroy{}
	multiQuery.Ini(buildFactoy.DbFactroy, "employee,department,joblevel")
	multiQuery.SelectFields("employee.id,employee.name,employee.beginDate,department.name,joblevel.name")
	multiQuery.Keys["joblevel.id"] = "employee.jobLevelId"
	multiQuery.Keys["department.id"] = "employee.departmentId"
	//multiQuery.TableAlias="employee_dpt"
	multiQuery.Build(buildFactoy.DbFactroy)
	multiQuery.LogDfc()

}
