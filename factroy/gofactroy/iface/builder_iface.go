package iface

import "gitee.com/ichub/go-factroy2024/factroy/dbfactroy"

/*
   @Title    文件名称: builder_iface.go
   @Description  描述: 代码工厂生成接口

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type BuilderIface interface {
	Build(dbfactroy *dbfactroy.DbFactroy) (dir, f, c string)
}
