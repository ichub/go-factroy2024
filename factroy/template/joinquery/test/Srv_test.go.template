package esservice

/*
    @Title  文件名称: {{.FileName}}Service
    @Description 描述: {{.Description}}

    @Author  作者: {{.Author}}  时间({{.DATETIME}})
    @Update  作者: {{.Author}}  时间({{.DATETIME}})

*/

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	basedto "icd/cms/base/dto"
	"icd/cms/dbcontent"
	"icd/cms/shop/dao"
	dto2 "icd/cms/shop/dto"
	"icd/cms/shop/service"
	"testing"
	"time"
)


// 数据库连接
var {{.ModelName}}MultiQryDbinst *gorm.DB

// 服务实例
var entityService{{.ModelName}}MultiQry esservice.{{.ESModelName}}MultiQryService

/*
     @title     函数名称: Setup
     @description      : 测试套前置脚本
     @auth      作者: {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        无

     @return    返回参数名:        无
*/
func {{.ModelName}}MultiQrySetup() {

	fmt.Println("setUp all tests")
	//mysql pg RoachPg MyRoach
	{{.ModelName}}MultiQryDbinst = {{.ModelName}}MultiQryIniDb("RoachPg")

}

/*
     @title     函数名称: Teardown
     @description      : 测试套后置脚本
     @auth      作者: {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        无

     @return    返回参数名:        无
*/
func {{.ModelName}}MultiQryTeardown() {
	fmt.Println("tearDown all tests")
}


/*
     @title     函数名称: init
     @description      : 初始化(TestMain 不能重复)
     @auth      作者    : {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        dbtype string
                                  取值：mysql, pg, RoachPg
     @return    返回参数名:        *gorm.DB
*/
func init(){
	//{{.ModelName}}Setup()
}



/*
     @title     函数名称 : TestMain
     @description       : 测试套全流程脚本
     @auth      作者     : {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        m *testing.M

     @return    返回参数名:        无
*/
/*
func TestMain(m *testing.M) {
	{{.ModelName}}MultiQrySetup()
	defer {{.ModelName}}MultiQryDbinst.Close()

	code := m.Run()

	{{.ModelName}}MultiQryTeardown()
	os.Exit(code)
}
*/


/*
     @title     函数名称: IniDb
     @description      : 连接数据库
     @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        dbtype string
                                  取值：mysql, pg, RoachPg
     @return    返回参数名:        *gorm.DB
*/
func {{.ModelName}}MultiQryIniDb(dbtype string) *gorm.DB {

	var dbinst *gorm.DB
	if dbtype == "mysql" {
		dbinst = dbcontent.InitDB_mysql(dbcontent.TestMysqlDBUrl())
	}

	if dbtype=="pg" {
		dbinst = dbcontent.InitDB_pg(dbcontent.TestPgDBUrl())
	}

	if dbtype=="RoachPg" {
		dbinst = dbcontent.InitDB_pg(dbcontent.TestRoachPgDBUrl())
	}

	if dbtype=="MyRoach" {
		dbinst = dbcontent.InitDB_pg(dbcontent.TestMyRoachDBUrl())
	}

	return dbinst
}



/*
 @title     函数名称: {{.ModelName}}MultiQryCheckJsonResult
 @description      : 检查结果返回码正确

 @auth      作者    :   {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名:  t *testing.T,
                        result *basedto.JsonResult,
                        code int32: 期望的返回码
 @return    返回参数名:  无

*/
func {{.ModelName}}MultiQryCheckJsonResult( t *testing.T, result *basedto.JsonResult, code int32 ) {
	if(result.Code != code){
		t.Errorf("JsonResult code %d != %d ",result.Code,code)
	}
	fmt.Println(result.String())
}


/*
 @title     函数名称: Test001_{{.ModelName}}MultiQry_Demo_JoinTables
 @description      : 测试接口-查询记录

 @auth      作者    :   {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名:  t *testing.T
 @return    返回参数名:  无

*/
func Test001_{{.ModelName}}MultiQry_Demo_JoinTables(t *testing.T) {
	t.Logf("start Demo_JoinTables ...")

	type Result struct {
		Id int32
		Name string
		//func (entity *Result) String() string {
		//	s, _ := json.Marshal(entity)
		//	return string(s)
		//}
	}

	var rs []Result
	db := dbinst.Table("employee").Select("employee.id,employee.name")
	db = db.Joins("JOIN department on department.id = employee.departmentId")
	db = db.Order("employee.id asc ").Limit(5).Find(&rs) 	//db = dbinst.Table("employee").Select("employee.*").Find(&m)
	if db.Error != nil {
		t.Errorf(db.Error.Error())
	}
	for _, v := range rs {
		s, _ := json.Marshal(v)
		fmt.Println(string(s))
	}

	fmt.Println(db.RecordNotFound())
}


/*
 @title     函数名称 :  Test002_{{.ModelName}}MultiQryFindByQueryParam_DAO
 @description       :  测试接口-更新非空字段,条件为主键{{.pkey}}

 @auth      作者     :  {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名:  t *testing.T
 @return    返回参数名:  无

*/

func Test002_{{.ModelName}}MultiQryFindByQueryParam_DAO(t *testing.T)  {

    param := dto2.{{.ModelName}}MultiQryParam{}
    param.Ini()
    param.PageSize = 3
    param.OrderBys="{{.queryTable}}.{{.pkeyField}}|desc"
    //param.Param.SetEmployeeId(1)
    param.Param.InRanges["{{.queryTable}}.{{.pkeyField}}"]=  "1,3"

    result, err := dao.InstMultiQry{{.ModelName}}DAO.FindByQueryParam(&param)
    if err != nil {
        fmt.Println(err.Error())
    }

    for _, v := range *result {
        fmt.Println("FindByQueryParam_DAO = " + v.String())
    }
    fmt.Println(param)


}

/*
 @title     函数名称 : Test003_{{.ModelName}}MultiQryCountByQueryParam_DAO
 @description       : 测试接口-根据主键{{.pkey}}删除记录
 @auth      作者     :   {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名:  t *testing.T
 @return    返回参数名:  无

*/
func Test003_{{.ModelName}}MultiQryCountByQueryParam_DAO(t *testing.T) {
    	param := dto2.{{.ModelName}}MultiQryParam{}
    	param.Ini()
    	param.PageSize = 5

    	cnt, _ := dao.InstMultiQry{{.ModelName}}DAO.CountByQueryParam(&param)

    	fmt.Println(cnt)


}

/*
 @title     函数名称  : Test004_{{.ModelName}}MultiQry_Service
 @description        : 测试接口-根据主键{{.pkey}}查找记录
 @auth      作者      :   {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名 :  t *testing.T
 @return    返回参数名 :  无

*/
func Test004_{{.ModelName}}MultiQry_Service(t *testing.T) {
	param := &dto2.{{.ModelName}}MultiQryParam{}
	param.Ini()
	param.PageSize = 3
	param.OrderBys="{{.queryTable}}.{{.pkeyField}}|desc"
	//param.Param.SetEmployeeId(1)
	param.Param.InRanges["{{.queryTable}}.{{.pkeyField}}"]=  "1,3"

	result := service.Inst{{.ModelName}}MultiQryService.Query(param)
	if result.Code != 200 {
		fmt.Println(result.Msg)
	}

	for _, v := range  result.Data {
		fmt.Println("Query_Service=" + v.String())
	}
	fmt.Println(param)
}



/*
 @title     函数名称: Test005_{{.ModelName}}MultiQryFindByIds_Service
 @description      : 测试接口-根据主键{{.pkey}} 查询多条记录, FindByIds("1,36,39")
 @auth      作者    :   {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名:  t *testing.T
 @return    返回参数名:  无

*/
func Test005_{{.ModelName}}MultiQryFindByIds_Service(t *testing.T) {

    result := service.Inst{{.ModelName}}MultiQryService.FindByIds("1,3")
    if result.Code != 200 {
        fmt.Println(result.Msg)
    }

    for _, v := range result.Data {
        fmt.Println("FindByIds_Service=" + v.String())
    }

}

/*
     @title     函数名称:  {{.ModelName}}MultiQryStrTime2Int
     @description      : 测试接口-通用查询
     @auth      作者    : {{.Author}} 时间: {{.DATETIME}}
     @param     输入参数名:        datetime string
     @return    返回参数名:        int64
*/

func {{.ModelName}}MultiQryStrTime2Int(datetime string) (int64) {
    	// 日期转化为时间戳
    	timeLayout := "2006-01-02 15:04:05"  //转化所需模板
    	loc, _ := time.LoadLocation("Local") //获取时区
    	tmp, _ := time.ParseInLocation(timeLayout, datetime, loc)
    	timestamp := tmp.Unix() //转化为时间戳 类型是int64
    	return timestamp
}
