package common

/*
   @Title    文件名称: module_define.go
   @Description  描述: 代码工厂模块定义文件

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
/*
	统一GO代码多层生成工具，
	author: leijianming
	2024-01-22
*/
type ModuleDefine struct {
	DbName    string
	ModelPath string
	TestPath  string
}

var Module = hrms

var (
	cms *ModuleDefine = &ModuleDefine{
		"cms",
		MODULE_NAME_SHOP,
		"icd/cms/shop-test",
	}

	hub *ModuleDefine = &ModuleDefine{
		"strapi-common-dev",
		"icd/common/shop",
		"icd/common/shop-dev",
	}
	hrms *ModuleDefine = &ModuleDefine{
		"hrms",
		"icd/hrms",
		"icd/hrms-test",
	}

	general *ModuleDefine = &ModuleDefine{
		"strapi-common-dev",
		"icd/common/shop",
		"icd/common/shop-dev",
	}
	System *ModuleDefine = &ModuleDefine{
		"system",
		"icd/system",
		"icd/system-test",
	}
)
