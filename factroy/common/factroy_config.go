package common

import (
	"encoding/json"
	"fmt"
	"github.com/duke-git/lancet/system"
	"github.com/spf13/viper"
	"gitee.com/ichub/go-factroy2024/common/base/utils/jsonutils"
)

/*
   @Title    文件名称: factroy_config.go
   @Description  描述: 代码工厂配置文件

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
const configfile = "/config/factroy_config.yml"

type Config struct {
	BasePath   string
	ConfigFile string
	Datasource struct {
		Dbtype string `yaml:"dbtype"`
		Dbname string `yaml:"dbname"`
		Host   string `yaml:"host"`
		Port   int    `yaml:"port"`

		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Charset  string `yaml:"charset"`
	}
}

func (entity *Config) String() string {
	s, _ := json.MarshalIndent(entity, "", "\t")
	return string(s)

}
func (config *Config) ReadConfigBy(basePath string, configFile string) {
	config.BasePath = basePath
	config.ConfigFile = configFile
	config.ReadConfig()
}

func (config *Config) printFileName() {
	fmt.Println(config.BasePath + config.ConfigFile)
}
func (config *Config) ReadConfig() {

	if config.BasePath == "" {
		config.BasePath = system.GetOsEnv("BasePath")
	}
	if config.ConfigFile == "" {
		config.ConfigFile = configfile
	}
	viper := viper.New()
	viper.SetConfigType("yaml")
	viper.SetConfigFile(config.BasePath + config.ConfigFile)
	config.printFileName()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
	}
	err = viper.Unmarshal(config)
	fmt.Println(err)
	fmt.Println("config=", jsonutils.ToJsonStr(config))

}
