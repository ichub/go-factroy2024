package common

/*
   @Title    文件名称: factroy_consts.go
   @Description  描述: 代码工厂常量文件

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
/* 根据这个切换不同系统的代码生成 */
const (
	MODULE_NAME_SHOP = "icd/cms/shop"
	MODULE_NAME_HRMS = "icd/hrms"

	MODULE_NAME_NOW = "icd/hub/shop"
	MODULE_NAME_NEW = MODULE_NAME_HRMS
)

const (
	DB_TYPE_MYSQL     = "mysql"
	DB_TYPE_POSTGRES  = "postgres"
	DB_TYPE_COCKROACH = "cockroach"
)

const (
	TEMPLATE_PATH = "/factroy/template/"

	OUTPUT_DIR = "/factroy/output/"

	LOG_FILE = "/opt/logs/factroy.log"
)
