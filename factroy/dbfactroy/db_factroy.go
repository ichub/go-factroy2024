package dbfactroy

import (
	"container/list"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"gitee.com/ichub/go-factroy2024/common/base/utils"
	"gitee.com/ichub/go-factroy2024/common/base/utils/email"
	"gitee.com/ichub/go-factroy2024/common/dbcontent"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy/metadata"
	"gitee.com/ichub/go-factroy2024/factroy/filefactroy"
	"log"
	"os"
	"strings"
)

/*
   @Title    文件名称: db_factroy.go
   @Description  描述: 代码工厂基础数据库工厂

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type DbFactroy struct {
	Author       string //作者
	DbConfig     *DbConfig
	ModuleDefine common.ModuleDefine

	DbType       string
	DbName       string  //数据库名
	Table        string  //表名
	TableComment *string //ModelNameWithTable bool

	Pkey     string
	PkeyType string

	Tables []string //genTables list

	GenDir     string
	GenDirTest string

	// tmp member var
	TestFiles       []string `json:"-"`
	StringFieldName string   `json:"-"`
	StringFieldLen  string   `json:"-"`

	AllTables []metadata.Tables   `json:"-"` //genTables all for CheckTableExist
	DirFileCS []metadata.DirFileC `json:"-"` //dir service content

	DbUrl string
	I     int `json:"-"`
}

//func (dbFactroy *DbFactroy) IniJoinQuery(tables string) (query *gofactroy.JoinQuery) {
//	query = &gofactroy.JoinQuery{}
//	query.Ini(dbFactroy, tables)
//	return
//}
func (dbFactroy *DbFactroy) IsMysql() bool {
	return dbFactroy.DbType == common.DB_TYPE_MYSQL

}

func (dbFactroy *DbFactroy) Ini() (dbinst *gorm.DB) {
	if dbFactroy.DbConfig != nil {

		dbFactroy.DbType = dbFactroy.DbConfig.DbType //common.DB_TYPE_MYSQL
		dbFactroy.DbUrl = dbFactroy.DbConfig.MakeDbUrl()
		dbFactroy.DbName = dbFactroy.DbConfig.DbName
	}
	metadata.MetadataContextInst.InitMap(dbFactroy.DbType)
	dbinst = dbFactroy.IniDb(dbFactroy.DbUrl)
	filefactroy.FileFactroyInst.ReCreateDirs()
	dbFactroy.IniLog()
	return
}

func (dbFactroy *DbFactroy) IniLog() *DbFactroy {
	logFile, err := os.OpenFile(common.LOG_FILE, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println("open log service failed, err:", err)
		return dbFactroy
	}
	log.SetOutput(logFile)
	return dbFactroy
}

func (dbFactroy *DbFactroy) IniDb(conn string) (dbinst *gorm.DB) {

	if dbFactroy.IsMysql() {
		filefactroy.FileFactroyInst.InitPathMysql()
		dbinst = dbcontent.InitDbMysql(conn)
	} else {
		filefactroy.FileFactroyInst.InitPostgres()
		dbinst = dbcontent.InitDbPostgres(conn)
	}
	return
}

func (dbFactroy *DbFactroy) SendEmail(subj, msg string) {
	emailReq := &email.SendRequest{
		Receivers: []string{"leijmdas_s180@163.com"},
		Subject:   subj,
		Message:   msg,
	}
	email.DefaultEmailHelper = &email.Client{}
	// retry send email
	err := email.DefaultEmailHelper.SendEmail(emailReq)
	if err != nil {
		fmt.Println(err.Error()) // panic(err.Error())
	}
}

func (dbFactroy *DbFactroy) makeBase(structName string) string {
	s := `type %sBase struct{
}`
	return fmt.Sprintf(s, structName)
}

func (dbFactroy *DbFactroy) printList(lst *list.List) {
	fmt.Println("\n")
	for i := lst.Front(); i != nil; i = i.Next() {
		fmt.Println(i.Value)
	}
}

func (dbFactroy *DbFactroy) Println() {
	fmt.Println(dbFactroy.ToString())
}

func (dbFactroy *DbFactroy) findPGTableComment() *metadata.Tables {
	sql := `SELECT 
		relname as table_name,
		obj_description(oid) as table_comment
		FROM pg_class
		WHERE relkind = 'r' and relname='%s'
	`
	sql = fmt.Sprintf(sql, dbFactroy.Table)
	var tables metadata.Tables
	e := dbcontent.GetDB().Model(&metadata.PkInfo{}).Raw(sql).Find(&tables).Error
	if e != nil {
		fmt.Println("***=" + e.Error())

	}
	return &tables
}

func (dbFactroy *DbFactroy) findPGColumnComment() []metadata.Columns {
	s := `SELECT 
    c.relname as TableName,
    col_description ( a.attrelid, a.attnum ) AS ColumnComment,
    format_type ( a.atttypid, a.atttypmod ) AS ColumnType,
    a.attname AS ColumnName 
	FROM pg_class AS c,    pg_attribute AS a
	WHERE    a.attrelid = c.oid   AND a.attnum >0
 			and c.relname = '%s'  `
	s = fmt.Sprintf(s, dbFactroy.Table)
	var cs []metadata.Columns
	e := dbcontent.GetDB().Model(&metadata.PkInfo{}).Raw(s).Find(&cs).Error
	if e != nil {
		fmt.Println("***=" + e.Error())
	}

	return cs
}

func (dbFactroy *DbFactroy) BuildInfo(gendir string) {
	fmt.Println("=================================")
	fmt.Println(dbFactroy.ToString())
	log.Println(dbFactroy.ToString())
	dbFactroy.GenDir = gendir

	fmt.Println("你产生的代码路径在：" + dbFactroy.GenDir)

	fmt.Println("=================================")
	fmt.Println()

}

func (dbFactroy *DbFactroy) BuildInfoTest(gendir string) {
	fmt.Println("=================================")

	dbFactroy.GenDirTest = gendir

	fmt.Println("你产生的测试代码路径在：" + dbFactroy.GenDirTest)
	fmt.Println("=================================")
	log.Println(dbFactroy.TestFiles)
	fmt.Println(strings.Join(dbFactroy.TestFiles, ","))
	fmt.Println()

}

func (tmp *DbFactroy) String() string {
	s, _ := json.Marshal(tmp)
	return string(s)

}

func (tmp *DbFactroy) ToString() string {
	s, _ := json.MarshalIndent(tmp, "", "    ")
	return string(s)

}

/*double	double	double
float	float	float
int32	int	int32	使用可变长编码方式。编码负数时不够高效——如果你的字段可能含有负数，那么请使用sint32。
int64	long	int64	使用可变长编码方式。编码负数时不够高效——如果你的字段可能含有负数，那么请使用sint64。
unit32	int[1]	unit32	总是4个字节。如果数值总是比总是比228大的话，这个类型会比uint32高效。
unit64	long[1]	unit64	总是8个字节。如果数值总是比总是比256大的话，这个类型会比uint64高效。
sint32	int	int32	使用可变长编码方式。有符号的整型值。编码时比通常的int32高效。
sint64	long	int64	使用可变长编码方式。有符号的整型值。编码时比通常的int64高效。
fixed32	int[1]	unit32
fixed64	long[1]	unit64	总是8个字节。如果数值总是比总是比256大的话，这个类型会比uint64高效。
sfixed32	int	int32	总是4个字节。
sfixed64	long	int64	总是8个字节。
bool	boolean	bool
string	String	string	一个字符串必须是UTF-8编码或者7-bit ASCII编码的文本。
bytes
*/

//dbcontent type->go type

func (dbFactroy *DbFactroy) makeModelColNewLst(columns *[]metadata.Columns, ifnil bool) *list.List {

	s := `
	entity.{{.colname}} = new({{.goType}})
	`
	lst := list.New()
	for _, v := range *columns {
		colname := utils.Case2Camel(v.ColumnName)

		goType := v.FindGoType(v.DataType)
		if v.ColumnName != dbFactroy.Pkey {
			vars := make(map[string]interface{})
			vars["goType"] = goType
			vars["colname"] = colname
			lst.PushBack(utils.ParseTemplateString(s, vars))
		}
	}
	dbFactroy.printList(lst)
	return lst
}

func (dbFactroy *DbFactroy) makeModelGo(columns *[]metadata.Columns) *list.List {
	sn := fmt.Sprintf(utils.Case2Camel(dbFactroy.Table))
	snbase := dbFactroy.makeBase(sn)
	lst := list.New()
	lst.PushBack(("/* 指定扩展结结构，单独存文件。生成时不会覆盖: */"))

	lst.PushBack(fmt.Sprintf("//type %sBase struct {ModelBase}", sn))
	comment := fmt.Sprintf("/*  %s  */", *dbFactroy.TableComment)
	lst.PushBack(comment)

	lst.PushBack(fmt.Sprintf("type %s%s struct {", "", utils.UcfirstCase2Camel(dbFactroy.Table)))
	//lst.PushBack(fmt.Sprintf("\tmodel.ParamBase" ))
	lst.PushBack(fmt.Sprintf("//\t%sBase", sn))
	lst.PushBack(fmt.Sprintf("//\tModelBase"))

	for _, v := range *columns {
		col_comment := fmt.Sprintf("\t/*  %s  */", v.ColumnComment)
		lst.PushBack(col_comment)
		//timeStr :=v.Birthday.Format("2006-01-02 15:04:05")
		dt := v.FindGoType(v.DataType)

		col_def := v.ColumnName + ";type:" + v.ColumnType
		if v.ColumnName == dbFactroy.Pkey {
			col_def = col_def + ";PRIMARY_KEY"
		}
		if dbFactroy.IsMysql() {
			col_def = col_def + fmt.Sprintf(";comment:'%s'", v.ColumnComment)
		}
		//if true || dbFactroy.Dbtype_mysql {
		if len(v.ColumnDefault) > 0 {
			if v.IfNumeric() {
				col_def = col_def + fmt.Sprintf(";default:%s", v.ColumnDefault)
			}
			if v.IfString() {
				col_def = col_def + fmt.Sprintf(";default:\\'%s\\'", v.ColumnDefault)
			}
		}
		//}

		json_def := v.ColumnName //utils.Lcfirst(utils.Case2Camel(v.ColumnName))
		if dt == "int64" {       //&& v.ColumnName == dbFactroy.Pkey
			json_def = json_def + ",string"
		}
		lst.PushBack(fmt.Sprintf("\t%s *%s `gorm:\"column:%s\" json:\"%s\"`", utils.Case2Camel(v.ColumnName), dt, col_def, json_def))
	}
	lst.PushBack("}")

	fmt.Println(snbase)
	for i := lst.Front(); i != nil; i = i.Next() {
		fmt.Println(i.Value)
	}
	return lst
}
func (dbFactroy *DbFactroy) makeModelBodyGo(columns *[]metadata.Columns) *list.List {

	lines := list.New()

	for _, c := range *columns {
		col_comment := fmt.Sprintf("\t/*  %s  */", c.ColumnComment)
		lines.PushBack(col_comment)
		dt := c.FindGoType(c.DataType)

		col_def := c.ColumnName + ";type:" + c.ColumnType
		if c.ColumnName == dbFactroy.Pkey {
			col_def = col_def + ";PRIMARY_KEY"
		}
		if dbFactroy.IsMysql() {
			col_def = col_def + fmt.Sprintf(";comment:'%s'", c.ColumnComment)
		}
		//if true || dbFactroy.Dbtype_mysql {
		if len(c.ColumnDefault) > 0 {
			if c.IfNumeric() {
				col_def = col_def + fmt.Sprintf(";default:%s", c.ColumnDefault)
			}
			if c.IfString() {
				col_def = col_def + fmt.Sprintf(";default:\\'%s\\'", c.ColumnDefault)
			}
		}
		json_def := c.ColumnName
		if dbFactroy.IsMysql() {
			json_def = utils.Lcfirst(utils.Case2Camel(c.ColumnName))
		}
		if dt == "int64" { //&& c.ColumnName == dbFactroy.Pkey
			json_def = json_def + ",string"
		}
		lines.PushBack(fmt.Sprintf("\t%s %s `gorm:\"column:%s\" json:\"%s\"`", utils.Case2Camel(c.ColumnName), dt, col_def, json_def))
	}

	for i := lines.Front(); i != nil; i = i.Next() {
		log.Println(i.Value)
	}
	return lines
}

func (dbFactroy *DbFactroy) makeModelDtoGo(columns *[]metadata.Columns) *list.List {

	lines := list.New()

	for _, c := range *columns {
		col_comment := fmt.Sprintf("\t/*  %s  */", c.ColumnComment)
		lines.PushBack(col_comment)
		dt := c.FindGoType(c.DataType)

		col_def := c.ColumnName + ";type:" + c.ColumnType
		if c.ColumnName == dbFactroy.Pkey {
			col_def = col_def + ";PRIMARY_KEY"
		}
		if dbFactroy.IsMysql() {
			col_def = col_def + fmt.Sprintf(";comment:'%s'", c.ColumnComment)
		}
		//if true || dbFactroy.Dbtype_mysql {
		if len(c.ColumnDefault) > 0 {
			if c.IfNumeric() {
				col_def = col_def + fmt.Sprintf(";default:%s", c.ColumnDefault)
			}
			if c.IfString() {
				col_def = col_def + fmt.Sprintf(";default:\\'%s\\'", c.ColumnDefault)
			}
		}
		json_def := c.ColumnName
		if dbFactroy.IsMysql() {
			json_def = utils.Lcfirst(utils.Case2Camel(c.ColumnName))
		}
		if dt == "int64" { //&& c.ColumnName == dbFactroy.Pkey
			json_def = json_def + ",string"
		}
		if dt == "bool" {
			dt = "string"
			lines.PushBack(fmt.Sprintf("\t%s %s `gorm:\"column:%s\" json:\"%s\"`", utils.Case2Camel(c.ColumnName), dt, col_def, json_def))

		} else {
			lines.PushBack(fmt.Sprintf("\t%s %s `gorm:\"column:%s\" json:\"%s\"`", utils.Case2Camel(c.ColumnName), dt, col_def, json_def))
		}
	}

	for i := lines.Front(); i != nil; i = i.Next() {
		log.Println(i.Value)
	}
	return lines
}

func (dbFactroy *DbFactroy) BuildModel() (cs *[]metadata.Columns, ms *list.List, ps *list.List, msdto *list.List) {

	cs = dbFactroy.FindColumns()
	ms = dbFactroy.makeModelBodyGo(cs)   // ms = dbFactroy.makeModelGo(cs)
	msdto = dbFactroy.makeModelDtoGo(cs) // ms = dbFactroy.makeModelGo(cs)
	ps = dbFactroy.MakeModelProtoBody(cs)

	return cs, ms, ps, msdto

}

func (dbFactroy *DbFactroy) MakeModelProto() *list.List {
	cs := dbFactroy.FindColumns()
	lst := dbFactroy.makeModelProto(cs)
	return lst

}
func (dbFactroy *DbFactroy) MakeModelProtoBody(columns *[]metadata.Columns) *list.List {

	sn := fmt.Sprintf(utils.Case2Camel(dbFactroy.Table))
	snbase := dbFactroy.makeBase(sn)
	lst := list.New()
	i := 5
	for _, v := range *columns {
		log.Println(v.String())
		dt := dbFactroy.FindProtoType(v.DataType)
		if dt == "" {
			dt = "string"
		}
		lst.PushBack(fmt.Sprintf("\t%s %s = %d;", dt, utils.Lcfirst(utils.Case2Camel(v.ColumnName)), i))
		i++
	}

	log.Println(snbase)
	for i := lst.Front(); i != nil; i = i.Next() {

		log.Println(i.Value)
	}
	return lst
}

func (dbFactroy *DbFactroy) makeModelProto(columns *[]metadata.Columns) *list.List {
	sn := fmt.Sprintf(utils.Case2Camel(dbFactroy.Table))
	snbase := dbFactroy.makeBase(sn)
	lst := list.New()
	//lst.PushBack(fmt.Sprintf("type %s%s struct {", Capitalize(tmp.DbName), Capitalize(tmp.Table)))
	lst.PushBack(fmt.Sprintf("message %sProto   {", utils.UcfirstCase2Camel(utils.Capitalize(dbFactroy.Table))))

	i := 1
	for _, v := range *columns {
		//timeStr :=cd v.Birthday.Format("2006-01-02 15:04:05")
		fmt.Println(v.String())
		dt := dbFactroy.FindProtoType(v.DataType)
		if dt == "" {
			log.Println("")
			dt = "string"
		}
		lst.PushBack(fmt.Sprintf("\t%s %s = %d;", dt, utils.Case2Camel(v.ColumnName), i))
		i = i + 1
	}
	lst.PushBack("}")
	fmt.Println(snbase)
	fmt.Println("\n")
	for i := lst.Front(); i != nil; i = i.Next() {

		fmt.Println(i.Value)
	}
	return lst
}

func (dbFactroy *DbFactroy) FindPgPkey(table string) []metadata.PkInfo {
	s := `SELECT
	pg_constraint.conname AS pkname,
	pg_attribute.attname AS colname,
	pg_type.typname AS typename
		FROM pg_constraint
		INNER JOIN pg_class ON pg_constraint.conrelid = pg_class.oid
		INNER JOIN pg_attribute ON pg_attribute.attrelid = pg_class.oid
		AND pg_attribute.attnum = pg_constraint.conkey [ 1 ]
		INNER JOIN pg_type ON pg_type.oid = pg_attribute.atttypid
	WHERE	pg_class.relname = '%s'
		AND pg_constraint.contype = 'p' `

	var pkInfos []metadata.PkInfo
	s = fmt.Sprintf(s, table)
	log.Println("Find PKey: " + s)
	e := dbcontent.GetDB().Model(&metadata.PkInfo{}).Raw(s).Find(&pkInfos).Error
	if e != nil {
		fmt.Println(e.Error())
	}

	log.Println("FindPgPkey: \n" + s)
	if len(pkInfos) == 0 {
		fmt.Println(fmt.Sprintf("pkInfos len=0 无主键（可能表%s不存在！） ", table))
	}
	log.Println(fmt.Sprintf("pkInfos: %d", len(pkInfos)))
	return pkInfos
}

func (dbFactroy *DbFactroy) buildIf(columns *[]metadata.Columns) *list.List {
	lst := list.New()
	s := `
		if param.Param.%s != nil {
			dbc = dbc.Where("%s=?", *param.Param.%s)
		}`
	for _, v := range *columns {
		cc := utils.Case2Camel(v.ColumnName)
		//timeStr := v.Birthday.Format("2006-01-02 15:04:05")
		if v.IfString() {
			s = `
			if param.Param.%s != nil ||  *param.Param.%s != "" {
				dbc = dbc.Where("%s = ?", *param.Param.%s)
			}`
			s = fmt.Sprintf(s, cc, cc, v.ColumnName, cc)
		} else if v.IfNumeric() {
			s = `
			if param.Param.%s != nil  ||  *param.Param.%s != 0 {
				dbc = dbc.Where("%s=?", *param.Param.%s)
			}`
			s = fmt.Sprintf(s, cc, cc, v.ColumnName, cc)
		} else {
			s = `
			if param.Param.%s != nil {
				dbc = dbc.Where("%s=?", *param.Param.%s)
			}`
			s = fmt.Sprintf(s, cc, v.ColumnName, cc)
		}

		lst.PushBack(s)
	}
	return lst
}

func (dbFactroy *DbFactroy) FindColumns() *[]metadata.Columns {

	var columns []metadata.Columns
	var sql string
	if dbFactroy.IsMysql() {
		sql = fmt.Sprintf(
			`select TABLE_NAME as table_name,
						 TABLE_SCHEMA as table_schema,
						 Column_Name  as column_name,
						 Data_Type  as data_type,
						 Column_Type  as column_type,  
						 Column_Key  as column_key ,
						 column_comment as column_comment ,
						 column_default as column_default 

					from information_schema.Columns 
					where table_schema='%s' and table_name='%s'`, dbFactroy.DbName, dbFactroy.Table)

	} else {
		//postgres sql
		sql = fmt.Sprintf(`select TABLE_NAME as table_name,
			 table_catalog as table_schema,
			 Column_Name  as column_name,
			 udt_name  as data_type,
			 crdb_sql_type  as column_type,  
			 character_maximum_length as char_max_len, 
			 ''  as column_key ,
			 column_default as column_default
			 from information_schema.Columns 
			 where table_catalog ='%s' and table_name='%s'`, dbFactroy.DbName, dbFactroy.Table)

	}
	fmt.Println(sql)
	log.Println(sql + "\n")
	e := dbcontent.GetDB().Raw(sql).Find(&columns).Error
	if e != nil {
		panic(e.Error())
	}

	if len(columns) == 0 {
		fmt.Println(dbFactroy.DbConfig.MakeDbUrl())
		panic(dbFactroy.Table + " columns len =0 (表不存在！)")
	}

	if dbFactroy.IsMysql() {
		for _, v := range columns { //timeStr := v.Birthday.Format("2006-01-02 15:04:05")

			if v.ColumnKey == "PRI" || v.ColumnName == "id" {
				dbFactroy.Pkey = v.ColumnName
				dbFactroy.PkeyType = v.DataType
			}
			if v.IfString() {
				dbFactroy.StringFieldName = v.ColumnName
				dbFactroy.StringFieldLen = v.CharMaxLen
			}
		}
	} else { // pgsql

		pks := dbFactroy.FindPgPkey(dbFactroy.Table)
		log.Println(fmt.Sprintf("pkInfos: %d", len(pks)))
		dbFactroy.Pkey = "id"
		dbFactroy.PkeyType = "int64"
		if len(pks) > 0 {
			dbFactroy.Pkey = pks[0].ColName
			dbFactroy.PkeyType = pks[0].TypeName
		}

		cscomment := dbFactroy.findPGColumnComment()
		for index := range columns { //timeStr := v.Birthday.Format("2006-01-02 15:04:05")
			for _, c := range cscomment {
				if columns[index].ColumnName == c.ColumnName {
					columns[index].ColumnComment = c.ColumnComment

				}
			}
			if columns[index].IfString() {
				dbFactroy.StringFieldName = columns[index].ColumnName
				dbFactroy.StringFieldLen = columns[index].CharMaxLen
			}
		}

	}

	return &columns
}

func (dbFactroy *DbFactroy) FindTableComment() {
	if dbFactroy.IsMysql() { //dbFactroy.TableComment == nil &&
		tbls := dbFactroy.FindTables()
		for _, v := range tbls {
			if dbFactroy.Table == v.TableName {
				dbFactroy.TableComment = &v.TableComment
				return
			}
		}
	}

	if !dbFactroy.IsMysql() { //dbFactroy.TableComment == nil &&
		tc := dbFactroy.findPGTableComment()
		dbFactroy.TableComment = &tc.TableComment

	}

}

func (dbFactroy *DbFactroy) CheckTableExist() bool {
	if len(dbFactroy.AllTables) == 0 {
		dbFactroy.AllTables = dbFactroy.FindTables()
	}
	for _, table := range dbFactroy.AllTables {
		if table.TableName == dbFactroy.Table {
			return true
		}
	}
	return false
}

func (dbFactroy *DbFactroy) FindTables() []metadata.Tables {
	var tables []metadata.Tables
	if dbFactroy.IsMysql() {
		sql := fmt.Sprintf(
			`select table_name as table_name ,
							 table_schema as table_schema ,
							 table_comment as table_comment
					 from information_schema.TABLES where table_schema='%s'  `, dbFactroy.DbName)
		err := dbcontent.GetDB().Raw(sql).Find(&tables).Error
		if err != nil {
			panic(err.Error() + " :" + sql)

		}
	} else {
		sql := fmt.Sprintf(
			`SELECT table_name as table_name,
						table_catalog  as table_schema
						FROM INFORMATION_SCHEMA.tables 
						WHERE table_schema='public' AND table_type IN ('BASE TABLE','VIEW') `)
		err := dbcontent.GetDB().Raw(sql).Find(&tables).Error
		if err != nil {
			panic(err.Error() + " :" + sql)

		}

	}
	if len(tables) > 0 {
		log.Println(len(tables))
		//json.Marshal(tables)
	}
	return tables
}

func (dbFactroy *DbFactroy) FindGoType(fieldType string) (goType string) {

	goType = metadata.MetadataContextInst.FindGoType(fieldType)
	return
}

func (dbFactroy *DbFactroy) FindProtoType(fieldType string) (pbType string) {

	pbType = metadata.MetadataContextInst.FindProtoType(fieldType)
	return
}
