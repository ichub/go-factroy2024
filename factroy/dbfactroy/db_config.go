package dbfactroy

import (
	"fmt"
	"gitee.com/ichub/go-factroy2024/factroy/common"
)

/*
   @Title    文件名称: db_config.go
   @Description  描述: 代码工厂数据库配置信息

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type DbConfig struct {
	DbType string
	DbName string

	Host     string
	Port     int32
	User     string
	Password string

	//dburl "root:leijmdas@163.comL@tcp(huawei.akunlong.top:13306)/%s?charset=utf8&mb4&parseTime=True&loc=Local",

}

//	DbUrl:        "host=192.168.13.235 port=26257 user=code  password=123456  dbname=" + base.Module.DbName + " sslmode=require",
func (dbConfig *DbConfig) MakeDbUrl() (dburl string) {

	if dbConfig.DbType == common.DB_TYPE_MYSQL {
		return dbConfig.MakeDbUrlMysql()
	}
	if len(dbConfig.Password) > 0 {
		return dbConfig.MakeDbUrlPostgresSsl()
	}
	return dbConfig.MakeDbUrlPostgres()
}

func (dbConfig *DbConfig) MakeDbUrlSsl() (dburl string) {

	if dbConfig.DbType == common.DB_TYPE_MYSQL {
		return dbConfig.MakeDbUrlMysql()
	}
	return dbConfig.MakeDbUrlPostgresSsl()
}
func (dbConfig *DbConfig) MakeDbUrlMysql() (dburl string) {
	dburl = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&mb4&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,

		dbConfig.DbName)
	return
}

func (dbConfig *DbConfig) MakeDbUrlPostgres() (dburl string) {
	//DbUrl:        "host=192.168.13.235 port=26257 user=code  password=123456  dbname=" + base.Module.DbName + " sslmode=require",
	//DbUrl:        "host=192.168.13.235 port=26257 user=code dbname=" + base.MODULE_MAP[base.APP].DbName  + " password=123456 sslmode=disable",
	//       "postgresql://test:123456@192.168.13.235:26257/" + base.Module.DbName + "?sslmode=require",
	//       "postgresql://root@192.168.14.153:26257/" + base.MODULE_MAP[base.APP].DbName + "?sslmode=disable",
	dburl = fmt.Sprintf(
		"postgresql://%s:%s@%s:%d/%s?sslmode=disable",

		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DbName)
	return
}
func (dbConfig *DbConfig) MakeDbUrlPostgresSsl() (dburl string) {
	//DbUrl:        "host=192.168.13.235 port=26257 user=code  password=123456  dbname=" + base.Module.DbName + " sslmode=require",
	//DbUrl:        "host=192.168.13.235 port=26257 user=code dbname=" + base.MODULE_MAP[base.APP].DbName  + " password=123456 sslmode=disable",
	dburl = fmt.Sprintf(
		"postgresql://%s:%s@%s:%d/%s?sslmode=require",

		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DbName)

	return
}
func (dbConfig *DbConfig) LogInfo() (dburl string) {
	dburl = dbConfig.MakeDbUrl()
	fmt.Println(dbConfig.MakeDbUrl())
	return
}
