package metadata

import "encoding/json"

/*
   @Title    文件名称: tables.go
   @Description  描述: 代码工厂元数据--表信息

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type Tables struct {
	TableSchema  string `gorm:"column:table_schema"`
	TableName    string `gorm:"column:table_name"`
	TableComment string `gorm:"column:table_comment"`
}

func (tables *Tables) ToString() string {
	s, _ := json.Marshal(tables)
	return string(s)

}
