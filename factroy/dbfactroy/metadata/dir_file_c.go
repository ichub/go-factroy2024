package metadata

/*
   @Title    文件名称: dir_file_c.go
   @Description  描述: 代码工厂元数据--目录，文件，代码行

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type DirFileC struct {
	Dir, File, C string //目录， 文件， 内容
}
