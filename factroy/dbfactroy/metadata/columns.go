package metadata

import (
	"encoding/json"
	"strings"
)

/*
   @Title    文件名称: coulmns.go
   @Description  描述: 代码工厂元数据--表字段

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type Columns struct {
	TableName     string `gorm:"column:table_name"`
	TableSchema   string `gorm:"column:table_schema"`
	ColumnName    string `gorm:"column:column_name"`
	DataType      string `gorm:"column:data_type"`
	ColumnType    string `gorm:"column:column_type"`
	ColumnKey     string `gorm:"column:column_key"`
	CharMaxLen    string `gorm:"column:char_max_len"`
	ColumnComment string `gorm:"column:column_comment"`
	ColumnDefault string `gorm:"column:column_default"`
}

func (c *Columns) String() string {
	s, _ := json.Marshal(c)
	return string(s)
}
func (c *Columns) ToString() string {
	s, _ := json.MarshalIndent(c, "", "    ")
	return string(s)
}
func (c *Columns) IfString() bool {
	return c.FindGoType(c.DataType) == "string"
}

func (cs *Columns) ReturnValue() (ReturnValue string) {
	ReturnValue = ""
	if cs.IfInt() || cs.IfNumeric() {
		ReturnValue = "return 0"
	} else if cs.IfBool() || cs.IfBitField() {
		ReturnValue = "return false"
	} else if cs.IfString() {
		ReturnValue = `return ""`
	} else if cs.IfTime() {
		ReturnValue = `return time.Time{}`
	} else if cs.IfLocalTimeInt() {
		ReturnValue = `return model.LocalTimeInt{}`
	} else if cs.IfLocalTimeUTCInt() {
		ReturnValue = `return model.LocalTimeUTCInt{}`
	} else if cs.IfLocalDateInt() {
		ReturnValue = `return model.LocalDateInt{}`
	} else if cs.IfLocalTime() {
		ReturnValue = `return model.LocalTime {}`
	} else if cs.IfLocalDate() {
		ReturnValue = `return model.LocalDate {}`
	}
	return
}

func (c *Columns) IfBool() bool {
	return c.FindGoType(c.DataType) == "bool"
}

func (c *Columns) IfBitField() bool {
	return strings.Contains(c.FindGoType(c.DataType), "BitField")
}

func (c *Columns) IfInt() bool {
	return strings.Contains(c.FindGoType(c.DataType), "int")
}
func (c *Columns) IfInt64() bool {
	return strings.Contains(c.FindGoType(c.DataType), "int64")
}
func (c *Columns) IfNumeric() bool {
	return c.IfInt() || strings.Contains(c.FindGoType(c.DataType), "decimal") || strings.Contains(c.FindGoType(c.DataType), "numeric") || strings.Contains(c.FindGoType(c.DataType), "float")

}

func (c *Columns) IfTime() bool {
	return strings.Contains(c.FindGoType(c.DataType), "time.Time")
}

func (c *Columns) IfLocalTimeInt() bool {
	return strings.Contains(c.FindGoType(c.DataType), "model.LocalTimeInt")
}
func (c *Columns) IfLocalTimeUTCInt() bool {
	return strings.Contains(c.FindGoType(c.DataType), "model.LocalTimeUTCInt")
}

func (c *Columns) IfLocalDateInt() bool {
	return strings.Contains(c.FindGoType(c.DataType), "model.LocalDateInt")
}

func (c *Columns) IfLocalTime() bool {
	return strings.Contains(c.FindGoType(c.DataType), "model.LocalTime")
}

func (c *Columns) IfLocalDate() bool {
	return strings.Contains(c.FindGoType(c.DataType), "model.LocalDate")
}

func (c *Columns) IfDateTime() bool {
	return strings.Contains(c.FindGoType(c.DataType), "time")
}

func (c *Columns) IfDate() bool {
	return strings.Contains(c.FindGoType(c.DataType), "date")
}

func (c *Columns) FindGoType(fieldType string) (goType string) {

	return MetadataContextInst.FindGoType(fieldType)
}
