package metadata

import "gitee.com/ichub/go-factroy2024/factroy/common"

/*
   @Title    文件名称: metadata_context.go
   @Description  描述: 代码工厂元数据--字典

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
var MetadataContextInst MetadataContext

type MetadataContext struct {
	dbtype              string
	DataTypeDB2GoMap    map[string]string /*创建集合 */
	DataTypeDB2ProtoMap map[string]string /*创建集合 */

}

func (context *MetadataContext) IsMysql() bool {
	return context.dbtype == common.DB_TYPE_MYSQL

}
func (context *MetadataContext) init() {
	context.InitMap("postgres")
}

func (context *MetadataContext) InitMap(dbtype string) {
	context.dbtype = dbtype
	context.DataTypeDB2GoMap = make(map[string]string)
	context.DataTypeDB2ProtoMap = make(map[string]string)
	if context.IsMysql() {
		context.initPbMap_Mysql()
		context.initGoMap_Mysql()
	} else {
		context.initPbMap()
		context.initGoMap()
	}

}

func (context *MetadataContext) FindGoType(fieldType string) (goType string) {
	goType = context.DataTypeDB2GoMap[fieldType]
	if goType == "" {
		goType = "string"
	}
	return goType
}

func (context *MetadataContext) FindProtoType(fieldType string) (pbType string) {
	pbType = context.DataTypeDB2ProtoMap[fieldType]
	if pbType == "" {
		pbType = "string"
	}
	return pbType
}

func (context *MetadataContext) initGoMap() {
	//type 2 gotype map
	DataTypeDB2GoMap := context.DataTypeDB2GoMap
	DataTypeDB2GoMap["char"] = "string"
	DataTypeDB2GoMap["varchar"] = "string"
	DataTypeDB2GoMap["text"] = "string"
	DataTypeDB2GoMap["bit"] = "model.BitField"
	DataTypeDB2GoMap["tinyint"] = "int8"
	DataTypeDB2GoMap["smallint"] = "int16"
	DataTypeDB2GoMap["mediumint"] = "int32"
	DataTypeDB2GoMap["int"] = "int32"
	DataTypeDB2GoMap["integer"] = "int32"
	DataTypeDB2GoMap["bigint"] = "int64"
	DataTypeDB2GoMap["double"] = "float64"
	DataTypeDB2GoMap["decimal"] = "float64"
	DataTypeDB2GoMap["numeric"] = "float64"

	DataTypeDB2GoMap["int32"] = "int32"
	DataTypeDB2GoMap["int64"] = "int64"
	DataTypeDB2GoMap["string"] = "string"
	DataTypeDB2GoMap["time"] = "time.Time"
	DataTypeDB2GoMap["date"] = "model.LocalDateInt"
	DataTypeDB2GoMap["datetime"] = "model.LocalTimeInt"
	DataTypeDB2GoMap["timestamp"] = "model.LocalTimeInt"
	DataTypeDB2GoMap["timestamptz"] = "model.LocalTimeUTCInt"

	DataTypeDB2GoMap["uuid"] = "string"

	DataTypeDB2GoMap["bool"] = "bool"
	DataTypeDB2GoMap["boolean"] = "bool"
	DataTypeDB2GoMap["serial2"] = "int16"
	DataTypeDB2GoMap["serial4"] = "int32"
	DataTypeDB2GoMap["serial8"] = "int64"
	DataTypeDB2GoMap["money"] = "float64"

	DataTypeDB2GoMap["int2"] = "int16"
	DataTypeDB2GoMap["int4"] = "int32"
	DataTypeDB2GoMap["int8"] = "int64"
	DataTypeDB2GoMap["decimal"] = "float64"
	DataTypeDB2GoMap["numeric"] = "float64"

	DataTypeDB2GoMap["float4"] = "float32"
	DataTypeDB2GoMap["float8"] = "float64"

}

func (context *MetadataContext) initPbMap() {
	context.DataTypeDB2ProtoMap["char"] = "string"
	context.DataTypeDB2ProtoMap["varchar"] = "string"
	context.DataTypeDB2ProtoMap["bit"] = "string"
	context.DataTypeDB2ProtoMap["tinyint"] = "int32"
	context.DataTypeDB2ProtoMap["smallint"] = "int32"
	context.DataTypeDB2ProtoMap["int"] = "int32"
	context.DataTypeDB2ProtoMap["bigint"] = "string"
	context.DataTypeDB2ProtoMap["double"] = "double"
	context.DataTypeDB2ProtoMap["decimal"] = "double"
	context.DataTypeDB2ProtoMap["numeric"] = "double"

	context.DataTypeDB2ProtoMap["time"] = "int64"
	context.DataTypeDB2ProtoMap["date"] = "int64"        // "google.protobuf.Timestamp"
	context.DataTypeDB2ProtoMap["datetime"] = "int64"    // "google.protobuf.Timestamp"
	context.DataTypeDB2ProtoMap["timestamp"] = "int64"   //" "google.protobuf.Timestamp"
	context.DataTypeDB2ProtoMap["timestamptz"] = "int64" //"google.protobuf.Timestamp"

	context.DataTypeDB2ProtoMap["uuid"] = "string"
	context.DataTypeDB2ProtoMap["boolean"] = "bool"
	context.DataTypeDB2ProtoMap["serial2"] = "int32"
	context.DataTypeDB2ProtoMap["serial4"] = "int32"
	// 64转string
	context.DataTypeDB2ProtoMap["serial8"] = "string" //"int64"
	context.DataTypeDB2ProtoMap["money"] = "double"
	context.DataTypeDB2ProtoMap["bool"] = "string"

	context.DataTypeDB2ProtoMap["int2"] = "int32"
	context.DataTypeDB2ProtoMap["int4"] = "int32"
	context.DataTypeDB2ProtoMap["int8"] = "string" //"int64"

	context.DataTypeDB2ProtoMap["float4"] = "float"
	context.DataTypeDB2ProtoMap["float8"] = "double"
	context.DataTypeDB2ProtoMap["model.LocalTime"] = "int64"
	context.DataTypeDB2ProtoMap["model.LocalDate"] = "int64"
	context.DataTypeDB2ProtoMap["model.LocalTimeInt"] = "int64"
	context.DataTypeDB2ProtoMap["model.LocalDateInt"] = "int64"
}

func (context *MetadataContext) initGoMap_Mysql() {
	DataTypeDB2GoMap := context.DataTypeDB2GoMap
	DataTypeDB2GoMap["char"] = "string"
	DataTypeDB2GoMap["varchar"] = "string"
	DataTypeDB2GoMap["text"] = "string"
	DataTypeDB2GoMap["bit"] = "model.BitField"
	DataTypeDB2GoMap["tinyint"] = "int8"
	DataTypeDB2GoMap["smallint"] = "int16"
	DataTypeDB2GoMap["mediumint"] = "int32"
	DataTypeDB2GoMap["int"] = "int32"
	DataTypeDB2GoMap["integer"] = "int32"
	DataTypeDB2GoMap["bigint"] = "int64"
	DataTypeDB2GoMap["double"] = "float64"
	DataTypeDB2GoMap["decimal"] = "float64"
	DataTypeDB2GoMap["numeric"] = "float64"

	DataTypeDB2GoMap["int32"] = "int32"
	DataTypeDB2GoMap["int64"] = "int64"
	DataTypeDB2GoMap["string"] = "string"
	DataTypeDB2GoMap["time"] = "time.Time"
	DataTypeDB2GoMap["date"] = "model.LocalDate"
	DataTypeDB2GoMap["datetime"] = "model.LocalTime"
	DataTypeDB2GoMap["timestamp"] = "model.LocalTime"
	DataTypeDB2GoMap["timestamptz"] = "model.LocalTime"

	DataTypeDB2GoMap["uuid"] = "string"
	DataTypeDB2GoMap["bool"] = "bool"
	DataTypeDB2GoMap["boolean"] = "bool"
	DataTypeDB2GoMap["serial2"] = "int16"
	DataTypeDB2GoMap["serial4"] = "int32"
	DataTypeDB2GoMap["serial8"] = "int64"
	DataTypeDB2GoMap["money"] = "float64"
	DataTypeDB2GoMap["int2"] = "int16"
	DataTypeDB2GoMap["int4"] = "int32"
	DataTypeDB2GoMap["int8"] = "int64"
	DataTypeDB2GoMap["decimal"] = "float64"
	DataTypeDB2GoMap["numeric"] = "float64"

	DataTypeDB2GoMap["float4"] = "float32"
	DataTypeDB2GoMap["float8"] = "float64"

}

func (context *MetadataContext) initPbMap_Mysql() {
	context.DataTypeDB2ProtoMap["char"] = "string"
	context.DataTypeDB2ProtoMap["varchar"] = "string"
	context.DataTypeDB2ProtoMap["bit"] = "string"
	context.DataTypeDB2ProtoMap["tinyint"] = "int32"
	context.DataTypeDB2ProtoMap["smallint"] = "int32"
	context.DataTypeDB2ProtoMap["int"] = "int32"
	context.DataTypeDB2ProtoMap["bigint"] = "string"
	context.DataTypeDB2ProtoMap["double"] = "double"
	context.DataTypeDB2ProtoMap["decimal"] = "double"
	context.DataTypeDB2ProtoMap["numeric"] = "double"

	context.DataTypeDB2ProtoMap["time"] = "int64"
	context.DataTypeDB2ProtoMap["date"] = "int64"
	context.DataTypeDB2ProtoMap["datetime"] = "int64"  // "google.protobuf.Timestamp"
	context.DataTypeDB2ProtoMap["timestamp"] = "int64" //" "google.protobuf.Timestamp"
	context.DataTypeDB2ProtoMap["timestamptz"] = "int64"

	context.DataTypeDB2ProtoMap["uuid"] = "string"
	context.DataTypeDB2ProtoMap["boolean"] = "bool"
	context.DataTypeDB2ProtoMap["serial2"] = "int32"
	context.DataTypeDB2ProtoMap["serial4"] = "int32"
	context.DataTypeDB2ProtoMap["serial8"] = "string"
	context.DataTypeDB2ProtoMap["money"] = "double"
	context.DataTypeDB2ProtoMap["bool"] = "string"

	context.DataTypeDB2ProtoMap["int2"] = "int32"
	context.DataTypeDB2ProtoMap["int4"] = "int32"
	context.DataTypeDB2ProtoMap["int8"] = "string"

	context.DataTypeDB2ProtoMap["float4"] = "float"
	context.DataTypeDB2ProtoMap["float8"] = "double"
	context.DataTypeDB2ProtoMap["model.LocalTime"] = "string"
	context.DataTypeDB2ProtoMap["model.LocalDate"] = "string"
	context.DataTypeDB2ProtoMap["model.LocalTimeInt"] = "string"
	context.DataTypeDB2ProtoMap["model.LocalDateInt"] = "string"
}

func (context *MetadataContext) cloneTags(tags map[string]string) map[string]string {
	cloneTags := make(map[string]string)
	for k, v := range tags {
		cloneTags[k] = v
	}
	return cloneTags
}
