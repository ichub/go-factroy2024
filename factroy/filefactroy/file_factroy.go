package filefactroy

import (
	"fmt"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"log"
	"os"
	"strings"
)

/*
   @Title    文件名称: file_factroy.go
   @Description  描述: 代码工厂代码文件生成

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/ /*
   @Title    文件名称: go_factroy.go
   @Description  描述: 代码工厂主文件

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type FileFactroy struct {
	FileTemplate
	TestFileFactroy

	Gendirs                               []string
	Gendir, GendirPbTool, GendirPbToolBin string

	GendirModel, GendirDto                string
	GendirDao, GendirInterface            string
	GendirService, GendirEsservice        string
	GendirProto, GendirGrpc               string
	GendirController, GendirRpcController string
}

var FileFactroyInst FileFactroy

func (fileFactroy *FileFactroy) InitPathMysql() {
	fileFactroy.initDir("gendir-mysql/")
	fileFactroy.initDirTest("gendir-mysql/")

}

func (fileFactroy *FileFactroy) InitPostgres() {

	fileFactroy.initDir("gendir-postgres/")
	fileFactroy.initDirTest("gendir-postgres/")
}

func (fileFactroy *FileFactroy) initDir(dbtype string) {

	workDir, _ := os.Getwd()
	fileFactroy.Gendir = workDir + common.OUTPUT_DIR + dbtype + common.Module.ModelPath

	fileFactroy.GendirPbToolBin = workDir + "/bin"
	fileFactroy.GendirPbTool = workDir + "/bin/proto" //basetest.MODULE_NAME

	fileFactroy.GendirModel = fileFactroy.Gendir + "/model"
	fileFactroy.GendirDto = fileFactroy.Gendir + "/dto"

	fileFactroy.GendirDao = fileFactroy.Gendir + "/dao"

	fileFactroy.GendirInterface = fileFactroy.Gendir + "/ruleiface"
	fileFactroy.GendirService = fileFactroy.Gendir + "/service"
	fileFactroy.GendirEsservice = fileFactroy.Gendir + "/esservice"

	fileFactroy.GendirProto = fileFactroy.Gendir + "/proto"
	fileFactroy.GendirGrpc = fileFactroy.Gendir + "/grpc"
	fileFactroy.GendirController = fileFactroy.Gendir + "/controller"
	fileFactroy.GendirRpcController = fileFactroy.Gendir + "/grpc.controller"

	fileFactroy.Gendirs = []string{fileFactroy.Gendir,
		fileFactroy.GendirProto,
		fileFactroy.GendirModel,
		fileFactroy.GendirDto,
		fileFactroy.GendirDao,
		fileFactroy.GendirService,
		fileFactroy.GendirEsservice,

		fileFactroy.GendirGrpc,
		fileFactroy.GendirController,
	}

}

func (fileFactroy *FileFactroy) RmDirs() {
	for _, v := range fileFactroy.Gendirs {
		_ = os.RemoveAll(v)
	}
	for _, v := range fileFactroy.GendirsTest {
		_ = os.RemoveAll(v)
	}

}

func (fileFactroy *FileFactroy) CreateDirs() {
	os.MkdirAll(fileFactroy.Gendir, os.ModePerm)

}

func (fileFactroy *FileFactroy) ReCreateDirs() {
	fileFactroy.RmDirs()
	fileFactroy.CreateDirs()
}

func (fileFactroy *FileFactroy) InfoPath() {
	fmt.Println("你产生的代码路径在：" + fileFactroy.Gendir)
	log.Println("你产生的代码路径在：" + fileFactroy.Gendir)
}

func (fileFactroy *FileFactroy) TransDir(path string, base string) string {
	base = strings.ToLower(base)
	path = strings.ReplaceAll(path, fileFactroy.Gendir, fileFactroy.Gendir+"/"+base)

	os.MkdirAll(fileFactroy.Gendir+"/"+base, os.ModeDir)
	os.MkdirAll(path, os.ModeDir)
	return path
}

func (fileFactroy *FileFactroy) WriteFileModule(dir string, filename string, cms string) {

	//cms = strings.ReplaceAll(cms, common.MODULE_NAME_SHOP, common.Module.ModelPath)
	cms = strings.ReplaceAll(cms, common.MODULE_NAME_NOW, common.Module.ModelPath)

	fileFactroy.WriteFile(dir, filename, cms)

}

func (fileFactroy *FileFactroy) WriteFileModuleRpc(dir, filename, cms string) {

	//cms = strings.ReplaceAll(cms, common.MODULE_NAME_SHOP, common.Module.ModelPath)
	cms = strings.ReplaceAll(cms, common.MODULE_NAME_NOW, common.Module.ModelPath)

	os.MkdirAll(dir, os.ModeDir)

	fileFactroy.WriteFile(dir, filename, cms)

}

func (fileFactroy *FileFactroy) ReadTemplateDto() []byte {

	return fileFactroy.ReadTemplateFile("Dto.template")

}

func (fileFactroy *FileFactroy) ReadTemplateModel() []byte {
	return fileFactroy.ReadTemplateFile("Model.template")

}

func (fileFactroy *FileFactroy) ReadTemplateDAO() []byte {

	return fileFactroy.ReadTemplateFile("Dao.template")

}
func (fileFactroy *FileFactroy) ReadTemplateRpc() []byte {

	return fileFactroy.ReadTemplateFile("Rpc.template")

}

func (fileFactroy *FileFactroy) ReadTemplateProto() []byte {

	return fileFactroy.ReadTemplateFile("Rpc.proto.template")

}

func (fileFactroy *FileFactroy) ReadTemplateServiceInterface() []byte {

	return fileFactroy.ReadTemplateFile("Interface.template")

}
func (fileFactroy *FileFactroy) ReadTemplateESService() []byte {

	return fileFactroy.ReadTemplateFile("ESService.template")

}

func (fileFactroy *FileFactroy) ReadTemplateService() []byte {

	return fileFactroy.ReadTemplateFile("Service.template")

}

func (fileFactroy *FileFactroy) ReadTemplateController() []byte {

	return fileFactroy.ReadTemplateFile("Controller.template")

}

func (fileFactroy *FileFactroy) ReadTemplateRpcController() []byte {

	return fileFactroy.ReadTemplateFile("Rpc.Controller.template")

}

func (fileFactroy *FileFactroy) ReadTestTemplateMqESSrv() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/test/Srv_test.go.template")

}
func (fileFactroy *FileFactroy) ReadTestTemplateMqSrv() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/test/Srv_test.go.template")

}
func (fileFactroy *FileFactroy) ReadTemplateMqProto() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/Rpc.proto.template")

}

func (fileFactroy *FileFactroy) ReadTemplateMqDAO() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/Dao.template")

}

func (fileFactroy *FileFactroy) ReadTemplateMqDTO() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/Dto.template")

}

func (fileFactroy *FileFactroy) ReadTemplateMqService() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/Service.template")

}
