package filefactroy

import (
	"fmt"
	"github.com/micro/go-micro/v2/util/log"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"io/ioutil"
	"os"
	"os/exec"
)

/*
   @Title    文件名称: file_template.go
   @Description  描述: 代码工厂文件生成基类

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type FileTemplate struct {
}

func (fileTemplate *FileTemplate) ReadTemplateFile(template string) []byte {
	dir, _ := os.Getwd()
	return fileTemplate.ReadFile(dir + common.TEMPLATE_PATH + template)

}
func (fileTemplate *FileTemplate) ExecPb() {
	cmd := exec.Command("pb")

	if err := cmd.Start(); err != nil { // 运行命令
		log.Fatal(err)

	}

}

func (fileTemplate *FileTemplate) PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func (fileTemplate *FileTemplate) ReadFile(filename string) []byte {

	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println(err.Error())
	}
	return b
}

func (fileTemplate *FileTemplate) WriteFile(dir string, filename string, cms string) {
	ise, _ := fileTemplate.PathExists(dir)
	if !ise {
		os.MkdirAll(dir, os.ModePerm)
	}
	ise, _ = fileTemplate.PathExists(dir)
	if !ise {
		panic(fmt.Sprintf("dir %s not exists!", dir))
	}
	f, err := os.Create(dir + "/" + filename)
	defer f.Close()

	if err != nil {
		fmt.Println(err)
		return
	}
	f.WriteString(cms)
}
