package filefactroy

import (
	"fmt"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"log"
	"os"
	"strings"
)

/*
   @Title    文件名称: testfile_factroy.go
   @Description  描述: 代码工厂测试文件生成

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
type TestFileFactroy struct {
	FileTemplate

	GendirTest string

	GendirControllerTest, GendirDaoTest    string
	GendirServiceTest, GendirEsserviceTest string
	GendirsTest                            []string
}

//var TestFileFactroyInst FileFactroy

func (fileFactroy *TestFileFactroy) InitPathMysql() {
	fileFactroy.initDirTest("gendir-mysql/")

}

func (fileFactroy *TestFileFactroy) InitPostgres() {

	fileFactroy.initDirTest("gendir-postgres/")
}

func (fileFactroy *TestFileFactroy) initDirTest(dbtype string) {
	workDir, _ := os.Getwd()
	fileFactroy.GendirTest = workDir + common.OUTPUT_DIR + dbtype + common.Module.TestPath

	fileFactroy.GendirControllerTest = fileFactroy.GendirTest + "/controller"
	fileFactroy.GendirDaoTest = fileFactroy.GendirTest + "/dao"
	fileFactroy.GendirServiceTest = fileFactroy.GendirTest + "/service"
	fileFactroy.GendirEsserviceTest = fileFactroy.GendirTest + "/esservice"

	fileFactroy.GendirsTest = []string{
		fileFactroy.GendirTest,
		fileFactroy.GendirDaoTest,
		fileFactroy.GendirServiceTest,
		fileFactroy.GendirEsserviceTest,
	}

	//os.MkdirAll(fileFactroy.Gendir, os.ModePerm)
	for _, v := range fileFactroy.GendirsTest {
		os.MkdirAll(v, os.ModePerm)
	}
}

func (fileFactroy *TestFileFactroy) RmDirs() {

	for _, v := range fileFactroy.GendirsTest {
		_ = os.RemoveAll(v)
		//fmt.Println(e)
	}

}

func (fileFactroy *TestFileFactroy) CreateDirs() {
	os.MkdirAll(fileFactroy.GendirTest, os.ModePerm)

}

func (fileFactroy *TestFileFactroy) ReCreateDirs() {
	fileFactroy.RmDirs()
	fileFactroy.CreateDirs()
}
func (fileFactroy *TestFileFactroy) InfoPath() {
	fmt.Println("你产生的代码路径在：" + fileFactroy.GendirTest)
	log.Println("你产生的代码路径在：" + fileFactroy.GendirTest)
}

func (fileFactroy *TestFileFactroy) TransDirTest(path string, base string) string {
	base = strings.ToLower(base)
	path = strings.ReplaceAll(path, fileFactroy.GendirTest, fileFactroy.GendirTest+"/"+base)

	os.MkdirAll(fileFactroy.GendirTest+"/"+base, os.ModeDir)
	os.MkdirAll(path, os.ModeDir)
	return path
}

func (fileFactroy *TestFileFactroy) WriteFileModule(dir string, filename string, cms string) {

	cms = strings.ReplaceAll(cms, common.MODULE_NAME_SHOP, common.Module.ModelPath)

	fileFactroy.WriteFile(dir, filename, cms)

}

func (fileFactroy *TestFileFactroy) ReadTestTemplateMqESSrv() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/test/Srv_test.go.template")

}
func (fileFactroy *TestFileFactroy) ReadTestTemplateMqSrv() []byte {

	return fileFactroy.ReadTemplateFile("joinquery/test/Srv_test.go.template")

}

func (fileFactroy *TestFileFactroy) ReadTestTemplateESSrv() []byte {

	return fileFactroy.ReadTemplateFile("test/Srv_test.go.template")

}
func (fileFactroy *TestFileFactroy) ReadTestTemplateController() []byte {

	return fileFactroy.ReadTemplateFile("test/Controller_test.go.template")

}

func (fileFactroy *TestFileFactroy) ReadTestTemplateSrv() []byte {

	return fileFactroy.ReadTemplateFile("test/Srv_test.go.template")
}

func (fileFactroy *TestFileFactroy) ReadTestTemplateDAO() []byte {

	return fileFactroy.ReadTemplateFile("test/Dao_test.go.template")

}
