package main

import (
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy"
)

/*
	有芯电子数据使用
*/
import (
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
	//rconn := "postgresql://root@192.168.4.119:26257/cms?sslmode=disable"

	dbFactroy := &dbfactroy.DbFactroy{
		DbType:       common.DB_TYPE_COCKROACH,
		DbName:       common.Module.DbName,
		Table:        "table_name1",
		Author:       "lxiansong",
		ModuleDefine: *common.Module,
		DbUrl:        "host=192.168.13.235 port=26257 user=code dbname=" + common.Module.DbName + " password=123456 sslmode=require",
	}

	dbinst := dbFactroy.Ini()
	defer dbinst.Close()
	tool := gofactroy.GetInstBy(dbFactroy)
	//var entity  model.TableName1
	//entity .AutoMigrate(dbcontent.GetDB())
	//单表生成
	tool.BuildTableDefault()

	func() {
		multiQuery := &gofactroy.JoinQueryFactroy{}
		multiQuery.Ini(dbFactroy, "cms_content,cms_column")
		multiQuery.SelectFields("cms_column.column_id,cms_content.content_id")
		multiQuery.Keys["cms_column.column_id"] = "cms_content.content_id"
		multiQuery.Build(dbFactroy)
	}()

	//全表生成
	tool.BuildTables()

}
