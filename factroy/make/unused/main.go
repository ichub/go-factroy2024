package main

import (
	"fmt"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy/gofactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy/iface"
)

func main() {
	fmt.Println(1)
	var builder iface.BuilderIface
	builder = &gofactroy.DaoFactroy{}

	dbConnInf := &dbfactroy.DbConfig{
		DbType:   common.DB_TYPE_MYSQL,
		DbName:   common.Module.DbName,
		User:     "root",
		Password: "leijmdas@163.comL",
		Host:     "huawei.akunlong.top",
		Port:     13306,
	}

	dbFactroy := &dbfactroy.DbFactroy{
		DbConfig:     dbConnInf,
		Author:       "leijianming@163.com",
		ModuleDefine: *common.Module,
	}

	builder.Build(dbFactroy)
}
