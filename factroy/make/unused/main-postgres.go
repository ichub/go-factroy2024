package main

import (
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy"
)

/*
	有芯电子数据使用
*/
import (
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {

	dbFactroy := &dbfactroy.DbFactroy{
		DbType:       common.DB_TYPE_COCKROACH,
		DbName:       common.System.DbName,
		Table:        "jobs",
		Author:       "leijmdas",
		ModuleDefine: *common.Module,
		DbUrl:        "host=192.168.14.58 port=26257 user=root  dbname=" + common.System.DbName + " sslmode=disable",
		//DbUrl:        "host=192.168.15.58 port=26257 user=root  password= dbname=" + base.Module.DbName + " sslmode=require",
		//DbUrl:        "host=192.168.13.235 port=26257 user=code dbname=" + base.MODULE_MAP[base.APP].DbName  + " password=123456 sslmode=disable",
		//DbUrl:      fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=require", "192.168.13.235", 26257, "code", base.DbName, "123456"),
	}
	//user=crtest password=Ichubtest1816 dbname=cms_test host=192.168.13.181 port=26257 sslmode=require

	dbinst := dbFactroy.Ini()
	defer dbinst.Close()

	//var entity model.{{.ModelPath}}
	//entity.AutoMigrate(dbcontent.GetDB())
	buildFactroy := gofactroy.GetInstBy(dbFactroy)
	//单表生成
	buildFactroy.BuildTableDefault()
	////全库生成
	buildFactroy.BuildTables()
	//cli := redisengine.Open()
	//defer cli.Close()
	//redisengine.TestRedis()
	//redisengine.FindUser("501aa3275b7f4f0cb6d0a6a3b1960261")
	////多表关联生成
	//func() {
	//	multiQuery := &gofactroy.JoinQuery{}
	//	multiQuery.Ini(dbFactroy, "cms_content,cms_column")
	//	multiQuery.SelectFields("cms_column.column_id,cms_content.content_id")
	//	multiQuery.Keys["cms_column.column_id"] = "cms_content.content_id"
	//	multiQuery.Build(dbFactroy)
	//}()

}

//## 查看yarn的配置
//yarn config list
//
//## 让strict-ssl配置设置为false
//yarn config set "strict-ssl" false -g
