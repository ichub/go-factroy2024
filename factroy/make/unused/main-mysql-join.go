package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
)

/*
 多表关联查询
*/
/*
    http://vrg123.com/
	idea golang工具激活码网站
*/

//	return fmt.Sprintf("host=%s port=%d shop-code=%s dbname=%s password=%s sslmode=disable",
//		"192.168.4.119", 5432, "postgres", dbname, "123456")
func main() {

	dbFactroy := &dbfactroy.DbFactroy{
		DbType:       common.DB_TYPE_MYSQL,
		DbName:       common.Module.DbName,
		Table:        "employee",
		Author:       "leijianming",
		ModuleDefine: *common.Module, //ModuleDefine:   basetest.MODULE_NAME,
		DbUrl: fmt.Sprintf("attend:123456@tcp(attend.akunlong.top:3306)/%s?charset=utf8&mb4&parseTime=True&loc=Local",
			common.Module.DbName),
	}

	dbinst := dbFactroy.Ini()
	defer dbinst.Close()
	goFactroy := gofactroy.GetInstBy(dbFactroy)
	//var entity model.TableName1
	//entity.AutoMigrate(dbcontent.GetDB())

	//单表生成
	goFactroy.BuildTableDefault()
	//全库生成
	goFactroy.BuildTables()

	//多表关联生成SERVICE
	func() {
		query := &gofactroy.JoinQueryFactroy{}
		query.Ini(dbFactroy, "cms_content,cms_column")
		query.SelectFields("cms_column.id,cms_content.content_id")
		query.Keys["cms_column.id"] = "cms_content.content_id"
		query.TableAlias = "CmsContentColumn"
		query.Build(dbFactroy)
	}()

}

//c := exec.Command("cmd", "/C", "go fmt -w -s", build2.Gendir+"\\table_name1\\model")
//c.Run()

//cli := redisengine.Open()
//defer cli.Close()
//redisengine.TestRedis()
//redisengine.FindUser("501aa3275b7f4f0cb6d0a6a3b1960261")
//fmt.Println(utils.Ip4())
