package main

import (
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

/*
   @Title    文件名称: build_mysql.go
   @Description  描述: 代码工厂执行

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
/**
@author:leijmdas
@date:2024-1-24
@explain: 生成mysql的代码
*/
/*
   http://vrg123.com/ idea golang工具激活码网站
*/

//pgDBUrlConn := func() string {
//	return fmt.Sprintf("host=%s port=%d shop-code=%s dbname=%s password=%s sslmode=disable",
//	"192.168.4.119", 5432, "postgres", dbname, "123456")
//}
var hwDbConfig = &dbfactroy.DbConfig{
	DbType: common.DB_TYPE_MYSQL,
	DbName: "hrms",

	Host:     "huawei.akunlong.top",
	Port:     13306,
	User:     "root",
	Password: "leijmdas@163.comL",
}
var icHubDbConfig = &dbfactroy.DbConfig{
	DbType: common.DB_TYPE_MYSQL,
	DbName: "hrms",

	Host:     "192.168.14.58",
	Port:     13306,
	User:     "root",
	Password: "root",
}

func main() {
	buildGoFactroy := gofactroy.GetInst()

	dbConfig := hwDbConfig //icHubDbConfig

	dbinst := buildGoFactroy.IniDb(dbConfig) //dbinst := buildGoFactroy.Ini()
	defer dbinst.Close()
	buildGoFactroy.BuildTables()
	buildGoFactroy.BuildJoinQuery()
	//buildGoFactroy.BuildTable("sys_dept")
	//zipResult := utils.ZipStr(jsonutils.ToJsonStr(dbConfig))
	//fmt.Println("zip result :", zipResult)
}
