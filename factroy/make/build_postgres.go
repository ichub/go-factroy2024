package main

import (
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitee.com/ichub/go-factroy2024/factroy/common"
	"gitee.com/ichub/go-factroy2024/factroy/dbfactroy"
	"gitee.com/ichub/go-factroy2024/factroy/gofactroy"
)

/*
   @Title    文件名称: build_postgres.go
   @Description  描述: 代码工厂执行

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/ /*
   @Title    文件名称: build_mysql.go
   @Description  描述: 代码工厂执行

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
/**
@author:leijmdas
@date:2024-1-24
@explain: 生成postgres的代码
*/
/*
   http://vrg123.com/ idea golang工具激活码网站
*/

//pgDBUrlConn := func() string {
//	return fmt.Sprintf("host=%s port=%d shop-code=%s dbname=%s password=%s sslmode=disable",
//	"192.168.4.119", 5432, "postgres", dbname, "123456")
//}	//DbUrl:        "host=192.168.13.235 port=26257 user=code dbname=" + base.MODULE_MAP[base.APP].DbName  + " password=123456 sslmode=disable",
//

func main() {
	buildFactroy := gofactroy.GetInst()
	dbConnInf := &dbfactroy.DbConfig{
		DbType:   common.DB_TYPE_COCKROACH,
		DbName:   common.System.DbName,
		User:     "root",
		Password: "",
		Host:     "192.168.14.58",
		Port:     26257,
	}
	dbinst := buildFactroy.IniDb(dbConnInf)
	defer dbinst.Close()
	buildFactroy.BuildTables()
	//	buildFactroy.BuildTable("jobs")

}
