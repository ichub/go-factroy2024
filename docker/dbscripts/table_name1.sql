create table table_name1
(
    id    int8 default nextval('public.table_name_id_seq'::STRING) not null constraint "primary" primary key,
    rowid      int8,
    vc         numeric(22, 2),
    name       varchar(223),
    time_hms   time(6),
    adate      date,
    atimestamp timestamp(6),
    adatetime  timestamptz(6),
    bit_field  bool,
    t          time(6)
);

