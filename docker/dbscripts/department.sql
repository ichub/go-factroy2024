/*
Navicat MySQL Data Transfer

Source Server         : attend.akunlong.top
Source Server Version : 50724
Source Host           : attend.akunlong.top:3306
Source Database       : vhr

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-08-01 15:27:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL DEFAULT 'none' COMMENT '编码',
  `name` varchar(32) DEFAULT NULL COMMENT '部门名称',
  `parentId` int(11) DEFAULT NULL,
  `depPath` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT b'1',
  `isParent` tinyint(1) DEFAULT '0',
  `mngrId` int(11) NOT NULL DEFAULT '0' COMMENT '负责人',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0-部门 1-班组',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '1', '股东会', '-1', '.1', '', '1', '0', '0');
INSERT INTO `department` VALUES ('4', '4', '董事会', '-1', '.1.4', '', '1', '0', '0');
INSERT INTO `department` VALUES ('5', '5', '总办', '4', '.1.4.5', '', '1', '0', '0');
INSERT INTO `department` VALUES ('8', '8', '财务部', '5', '.1.4.5.8', '', '0', '0', '0');
INSERT INTO `department` VALUES ('78', '78', '市场部', '5', '.1.4.5.78', '', '1', '0', '0');
INSERT INTO `department` VALUES ('81', '81', '华北市场部', '78', '.1.4.5.78.81', '', '1', '0', '0');
INSERT INTO `department` VALUES ('82', '82', '华南市场部', '78', '.1.4.5.78.82', '', '0', '0', '0');
INSERT INTO `department` VALUES ('85', '85', '石家庄市场部', '81', '.1.4.5.78.81.85', '', '0', '0', '0');
INSERT INTO `department` VALUES ('86', '86', '西北市场部', '78', '.1.4.5.78.86', '', '1', '0', '0');
INSERT INTO `department` VALUES ('87', '87', '西安市场', '86', '.1.4.5.78.86.87', '', '1', '0', '0');
INSERT INTO `department` VALUES ('89', '89', '莲湖区市场', '87', '.1.4.5.78.86.87.89', '', '0', '0', '0');
INSERT INTO `department` VALUES ('91', '91', '技术部', '5', '.1.4.5.91', '', '0', '0', '0');
INSERT INTO `department` VALUES ('92', '92', '运维部', '5', '.1.4.5.92', '', '1', '0', '0');
INSERT INTO `department` VALUES ('93', '93', '运维1部', '92', '.1.4.5.92.93', '', '0', '0', '0');
INSERT INTO `department` VALUES ('94', '941', '运维2部', '92', '.1.4.5.92.94', '', '0', '0', '0');
