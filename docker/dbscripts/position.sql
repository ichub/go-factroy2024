/*
Navicat MySQL Data Transfer

Source Server         : attend.akunlong.top
Source Server Version : 50724
Source Host           : attend.akunlong.top:3306
Source Database       : vhr

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-08-01 15:29:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '职位',
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES ('29', '技术总监', '2018-01-11 21:13:42', '1');
INSERT INTO `position` VALUES ('30', '运营总监', '2018-01-11 21:13:48', '1');
INSERT INTO `position` VALUES ('31', '市场总监', '2018-01-11 00:00:00', '1');
INSERT INTO `position` VALUES ('33', '研发工程师', '2018-01-14 00:00:00', '0');
INSERT INTO `position` VALUES ('34', '运维工程师', '2018-01-14 16:11:41', '1');
INSERT INTO `position` VALUES ('36', 'Java研发经理', '2019-10-01 00:00:00', '1');
