/*
Navicat PGSQL Data Transfer

Source Server         : 192.168.4.119postgres
Source Server Version : 90426
Source Host           : 192.168.4.119:5432
Source Database       : cms
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90426
File Encoding         : 65001

Date: 2021-07-31 12:53:18
*/


-- ----------------------------
-- Table structure for cms_layout_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."cms_layout_type";
CREATE TABLE "public"."cms_layout_type"
(
    "layout_type_id" int8 NOT NULL,
    "shop_id" int8 NOT NULL,
    "name"       varchar(64) COLLATE "default" NOT NULL,
    "created_at" date,
    "created_by" int8,
    "is_default" bool,
    "date1"      date,
    "datetime1"  time(6),
    "active"     bool
)
WITH (OIDS= FALSE)
;

-- ----------------------------
-- Records of cms_layout_type
-- ----------------------------
INSERT INTO "public"."cms_layout_type"
VALUES ('1', '1', '中山之郎', '2021-07-22', '1', 't', null, null, null);
INSERT INTO "public"."cms_layout_type"
VALUES ('2', '99', '中山之郎', '2021-07-31', null, null, null, null, null);
INSERT INTO "public"."cms_layout_type"
VALUES ('3', '99', '中山之郎', '2021-07-31', null, 'f', null, null, 't');
INSERT INTO "public"."cms_layout_type"
VALUES ('4', '99', '中山之郎', '2021-07-31', null, 'f', null, null, 't');
INSERT INTO "public"."cms_layout_type"
VALUES ('80', '99', '中山之郎', '2021-07-31', null, 'f', null, null, 't');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table cms_layout_type
-- ----------------------------
ALTER TABLE "public"."cms_layout_type"
    ADD PRIMARY KEY ("layout_type_id");
