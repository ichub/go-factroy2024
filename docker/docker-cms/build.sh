#!/bin/sh
# Enable the go modules feature
export GO111MODULE=on
# Set the GOPROXY environment variable
export GOPROXY=https://goproxy.io


docker-compose build
docker-compose down
docker-compose up -d
